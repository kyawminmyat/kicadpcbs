EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "A9 RTC TM1637"
Date "2020-11-23"
Rev "0"
Comp "Everything Electronic"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Mechanical:MountingHole H1
U 1 1 60071B76
P 10400 700
F 0 "H1" H 10500 746 50  0000 L CNN
F 1 "MountingHole" H 10500 655 50  0000 L CNN
F 2 "MountingHole:MountingHole_3mm_Pad_Via" H 10400 700 50  0001 C CNN
F 3 "~" H 10400 700 50  0001 C CNN
	1    10400 700 
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 600722EB
P 10400 950
F 0 "H2" H 10500 996 50  0000 L CNN
F 1 "MountingHole" H 10500 905 50  0000 L CNN
F 2 "MountingHole:MountingHole_3mm_Pad_Via" H 10400 950 50  0001 C CNN
F 3 "~" H 10400 950 50  0001 C CNN
	1    10400 950 
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 600723F7
P 10400 1200
F 0 "H3" H 10500 1246 50  0000 L CNN
F 1 "MountingHole" H 10500 1155 50  0000 L CNN
F 2 "MountingHole:MountingHole_3mm_Pad_Via" H 10400 1200 50  0001 C CNN
F 3 "~" H 10400 1200 50  0001 C CNN
	1    10400 1200
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 60072588
P 10400 1450
F 0 "H4" H 10500 1496 50  0000 L CNN
F 1 "MountingHole" H 10500 1405 50  0000 L CNN
F 2 "MountingHole:MountingHole_3mm_Pad_Via" H 10400 1450 50  0001 C CNN
F 3 "~" H 10400 1450 50  0001 C CNN
	1    10400 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 1600 6050 1650
Wire Wire Line
	6250 1600 6050 1600
Wire Wire Line
	5950 1500 5950 1650
Wire Wire Line
	6250 1500 5950 1500
Wire Wire Line
	5850 1400 5850 1650
Wire Wire Line
	6250 1400 5850 1400
Text Label 6250 1600 2    50   ~ 0
DIG9
Text Label 6250 1500 2    50   ~ 0
DIG8
Text Label 6250 1400 2    50   ~ 0
DIG7
Wire Wire Line
	5250 1600 5450 1600
Wire Wire Line
	5450 1600 5450 1650
Text Label 5250 1600 0    50   ~ 0
dp2
Wire Wire Line
	7250 1600 7250 1650
Wire Wire Line
	7450 1600 7250 1600
Wire Wire Line
	7150 1500 7150 1650
Wire Wire Line
	7450 1500 7150 1500
Wire Wire Line
	7050 1400 7050 1650
Wire Wire Line
	7450 1400 7050 1400
Text Label 7450 1600 2    50   ~ 0
DIG12
Text Label 7450 1500 2    50   ~ 0
DIG11
Text Label 7450 1400 2    50   ~ 0
DIG10
Wire Wire Line
	6450 1600 6650 1600
Wire Wire Line
	6650 1600 6650 1650
Text Label 6450 1600 0    50   ~ 0
dp2
Wire Wire Line
	3500 1600 3500 1650
Wire Wire Line
	3700 1600 3500 1600
Wire Wire Line
	3400 1500 3400 1650
Wire Wire Line
	3700 1500 3400 1500
Wire Wire Line
	3300 1400 3300 1650
Wire Wire Line
	3700 1400 3300 1400
Text Label 3700 1600 2    50   ~ 0
DIG3
Text Label 3700 1500 2    50   ~ 0
DIG2
Text Label 3700 1400 2    50   ~ 0
DIG1
Wire Wire Line
	2700 1600 2900 1600
Wire Wire Line
	2900 1600 2900 1650
Text Label 2700 1600 0    50   ~ 0
dp1
Wire Wire Line
	4700 1600 4700 1650
Wire Wire Line
	4900 1600 4700 1600
Wire Wire Line
	4600 1500 4600 1650
Wire Wire Line
	4900 1500 4600 1500
Wire Wire Line
	4500 1400 4500 1650
Wire Wire Line
	4900 1400 4500 1400
Text Label 4900 1600 2    50   ~ 0
DIG6
Text Label 4900 1500 2    50   ~ 0
DIG5
Text Label 4900 1400 2    50   ~ 0
DIG4
Wire Wire Line
	3900 1600 4100 1600
Wire Wire Line
	4100 1600 4100 1650
Text Label 3900 1600 0    50   ~ 0
dp1
Wire Wire Line
	4100 2600 4100 2450
Text Label 4100 2600 1    50   ~ 0
a1
Text Label 3100 3050 0    50   ~ 0
a1
Text Label 3100 3150 0    50   ~ 0
b1
Text Label 3100 3250 0    50   ~ 0
c1
Text Label 3100 3350 0    50   ~ 0
d1
Text Label 3100 3450 0    50   ~ 0
e1
Text Label 3100 3550 0    50   ~ 0
f1
Text Label 3100 3650 0    50   ~ 0
g1
Text Label 3100 3750 0    50   ~ 0
dp1
$Comp
L power:GND #PWR0101
U 1 1 5FC24826
P 2800 3000
F 0 "#PWR0101" H 2800 2750 50  0001 C CNN
F 1 "GND" H 2800 2850 50  0000 C CNN
F 2 "" H 2800 3000 50  0001 C CNN
F 3 "" H 2800 3000 50  0001 C CNN
	1    2800 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 2950 2800 2950
Wire Wire Line
	2800 2950 2800 3000
Wire Wire Line
	3100 3050 3250 3050
Wire Wire Line
	3100 3150 3250 3150
Wire Wire Line
	3100 3250 3250 3250
Wire Wire Line
	3100 3350 3250 3350
Wire Wire Line
	3100 3450 3250 3450
Wire Wire Line
	3100 3550 3250 3550
Wire Wire Line
	3100 3650 3250 3650
Wire Wire Line
	3100 3750 3250 3750
Text Label 3050 3850 0    50   ~ 0
DIG6
Wire Wire Line
	3050 3850 3250 3850
$Comp
L BA56-12EWA:BA56-12EWA DS2
U 1 1 5FC3216C
P 3200 2050
F 0 "DS2" H 2750 2700 50  0000 L CNN
F 1 "BA56-12EWA" H 2750 2600 50  0000 L CNN
F 2 "BA56-12EWA(3Digit7Segment):BA56-12EWA" H 3200 2050 50  0001 L BNN
F 3 "" H 3200 2050 50  0001 L BNN
	1    3200 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 2600 3500 2450
Wire Wire Line
	3400 2600 3400 2450
Wire Wire Line
	3300 2600 3300 2450
Wire Wire Line
	3200 2600 3200 2450
Wire Wire Line
	3100 2600 3100 2450
Wire Wire Line
	3000 2600 3000 2450
Wire Wire Line
	2900 2600 2900 2450
Text Label 3500 2600 1    50   ~ 0
g1
Text Label 3400 2600 1    50   ~ 0
f1
Text Label 3300 2600 1    50   ~ 0
e1
Text Label 3200 2600 1    50   ~ 0
d1
Text Label 3100 2600 1    50   ~ 0
c1
Text Label 3000 2600 1    50   ~ 0
b1
Text Label 2900 2600 1    50   ~ 0
a1
Text Label 5100 4950 2    50   ~ 0
SCL1
Text Label 5100 5050 2    50   ~ 0
SCL2
Text Label 5100 5150 2    50   ~ 0
SDA2
Wire Wire Line
	7700 4450 7700 4550
$Comp
L power:+5V #PWR06
U 1 1 5FD943E5
P 7700 4450
F 0 "#PWR06" H 7700 4300 50  0001 C CNN
F 1 "+5V" H 7715 4623 50  0000 C CNN
F 2 "" H 7700 4450 50  0001 C CNN
F 3 "" H 7700 4450 50  0001 C CNN
	1    7700 4450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR05
U 1 1 5FD9374F
P 7250 4950
F 0 "#PWR05" H 7250 4700 50  0001 C CNN
F 1 "GND" H 7250 4800 50  0000 C CNN
F 2 "" H 7250 4950 50  0001 C CNN
F 3 "" H 7250 4950 50  0001 C CNN
	1    7250 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5850 5400 6900 5400
Wire Wire Line
	5850 5250 6650 5250
Wire Wire Line
	5850 5100 6400 5100
Wire Wire Line
	5850 4950 6150 4950
Text Label 5850 5400 0    50   ~ 0
SDA2
Text Label 5850 5250 0    50   ~ 0
SCL2
Text Label 5850 5100 0    50   ~ 0
SCL1
Text Label 5850 4950 0    50   ~ 0
SDA1
$Comp
L Device:C C4
U 1 1 5FD933AE
P 7700 4700
F 0 "C4" H 7815 4746 50  0000 L CNN
F 1 "104" H 7815 4655 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W3.5mm_P5.00mm" H 7738 4550 50  0001 C CNN
F 3 "~" H 7700 4700 50  0001 C CNN
	1    7700 4700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR07
U 1 1 5FD93CAD
P 7700 4950
F 0 "#PWR07" H 7700 4700 50  0001 C CNN
F 1 "GND" H 7700 4800 50  0000 C CNN
F 2 "" H 7700 4950 50  0001 C CNN
F 3 "" H 7700 4950 50  0001 C CNN
	1    7700 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	7700 4850 7700 4950
Wire Wire Line
	4850 5150 5100 5150
Wire Wire Line
	4850 5050 5100 5050
Wire Wire Line
	4850 4950 5100 4950
$Comp
L DRIVER-TM1636_DIP18_:TM1637 U2
U 1 1 5FC44ED1
P 6350 3350
F 0 "U2" H 6350 4015 50  0000 C CNN
F 1 "TM1637" H 6350 3924 50  0000 C CNN
F 2 "Package_SO:SOP-20_7.5x12.8mm_P1.27mm" H 6400 3900 50  0001 C CNN
F 3 "" H 6350 3350 50  0001 C CNN
	1    6350 3350
	1    0    0    -1  
$EndComp
Text Label 5650 3550 0    50   ~ 0
f2
Text Label 5650 3650 0    50   ~ 0
g2
Text Label 5650 3750 0    50   ~ 0
dp2
Wire Wire Line
	5650 3550 5800 3550
Wire Wire Line
	5650 3650 5800 3650
Wire Wire Line
	5650 3750 5800 3750
Text Label 5550 3850 0    50   ~ 0
DIG12
Text Label 7150 3850 2    50   ~ 0
DIG11
Text Label 7150 3750 2    50   ~ 0
DIG10
Text Label 7100 3650 2    50   ~ 0
DIG9
Text Label 7100 3550 2    50   ~ 0
DIG8
Wire Wire Line
	7100 3650 6900 3650
Wire Wire Line
	7100 3550 6900 3550
Wire Wire Line
	6900 3750 7150 3750
Wire Wire Line
	6900 3850 7150 3850
Wire Wire Line
	5550 3850 5800 3850
$Comp
L power:+5V #PWR01
U 1 1 5FCBDE72
P 5300 4600
F 0 "#PWR01" H 5300 4450 50  0001 C CNN
F 1 "+5V" H 5315 4773 50  0000 C CNN
F 2 "" H 5300 4600 50  0001 C CNN
F 3 "" H 5300 4600 50  0001 C CNN
	1    5300 4600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 5FCBE29E
P 5300 4800
F 0 "#PWR02" H 5300 4550 50  0001 C CNN
F 1 "GND" H 5300 4650 50  0000 C CNN
F 2 "" H 5300 4800 50  0001 C CNN
F 3 "" H 5300 4800 50  0001 C CNN
	1    5300 4800
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5FD70630
P 6150 4700
F 0 "R1" H 6220 4746 50  0000 L CNN
F 1 "10k" H 6220 4655 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 6080 4700 50  0001 C CNN
F 3 "~" H 6150 4700 50  0001 C CNN
	1    6150 4700
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5FD721AF
P 6400 4700
F 0 "R2" H 6470 4746 50  0000 L CNN
F 1 "10k" H 6470 4655 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 6330 4700 50  0001 C CNN
F 3 "~" H 6400 4700 50  0001 C CNN
	1    6400 4700
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5FD72359
P 6650 4700
F 0 "R3" H 6720 4746 50  0000 L CNN
F 1 "10k" H 6720 4655 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 6580 4700 50  0001 C CNN
F 3 "~" H 6650 4700 50  0001 C CNN
	1    6650 4700
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 5FD725EB
P 6900 4700
F 0 "R4" H 6970 4746 50  0000 L CNN
F 1 "10k" H 6970 4655 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 6830 4700 50  0001 C CNN
F 3 "~" H 6900 4700 50  0001 C CNN
	1    6900 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 4950 6150 4850
Wire Wire Line
	6400 5100 6400 4850
Wire Wire Line
	6650 5250 6650 4850
Wire Wire Line
	6900 5400 6900 4850
$Comp
L power:+5V #PWR03
U 1 1 5FD82070
P 6550 4350
F 0 "#PWR03" H 6550 4200 50  0001 C CNN
F 1 "+5V" H 6565 4523 50  0000 C CNN
F 2 "" H 6550 4350 50  0001 C CNN
F 3 "" H 6550 4350 50  0001 C CNN
	1    6550 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 4550 6150 4350
Wire Wire Line
	6150 4350 6400 4350
Wire Wire Line
	6400 4550 6400 4350
Connection ~ 6400 4350
Wire Wire Line
	6400 4350 6550 4350
Wire Wire Line
	6650 4550 6650 4350
Wire Wire Line
	6650 4350 6550 4350
Connection ~ 6550 4350
Wire Wire Line
	6900 4550 6900 4350
Wire Wire Line
	6900 4350 6650 4350
Connection ~ 6650 4350
$Comp
L Device:C C2
U 1 1 5FD924AE
P 7250 4700
F 0 "C2" H 7365 4746 50  0000 L CNN
F 1 "104" H 7365 4655 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W3.5mm_P5.00mm" H 7288 4550 50  0001 C CNN
F 3 "~" H 7250 4700 50  0001 C CNN
	1    7250 4700
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR04
U 1 1 5FD940B1
P 7250 4450
F 0 "#PWR04" H 7250 4300 50  0001 C CNN
F 1 "+5V" H 7265 4623 50  0000 C CNN
F 2 "" H 7250 4450 50  0001 C CNN
F 3 "" H 7250 4450 50  0001 C CNN
	1    7250 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	7250 4450 7250 4550
Wire Wire Line
	7250 4850 7250 4950
Wire Wire Line
	4850 4750 5300 4750
Wire Wire Line
	5300 4750 5300 4800
Wire Wire Line
	4850 4650 5300 4650
Wire Wire Line
	5300 4650 5300 4600
Wire Wire Line
	4850 4850 5100 4850
Text Label 5100 4850 2    50   ~ 0
SDA1
$Comp
L Connector_Generic:Conn_01x06 J1
U 1 1 5FCBCC01
P 4650 4950
F 0 "J1" H 4568 4425 50  0000 C CNN
F 1 "Conn_01x06" H 4568 4516 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 4650 4950 50  0001 C CNN
F 3 "~" H 4650 4950 50  0001 C CNN
	1    4650 4950
	-1   0    0    1   
$EndComp
Wire Wire Line
	4350 3150 4550 3150
Wire Wire Line
	4350 3250 4550 3250
Wire Wire Line
	6900 3250 7100 3250
Wire Wire Line
	6900 3150 7100 3150
Text Label 7100 3150 2    50   ~ 0
SCL2
Text Label 7100 3250 2    50   ~ 0
SDA2
Text Label 4550 3150 2    50   ~ 0
SCL1
Text Label 4550 3250 2    50   ~ 0
SDA1
Text Label 5450 2600 1    50   ~ 0
a2
Text Label 5550 2600 1    50   ~ 0
b2
Text Label 5650 2600 1    50   ~ 0
c2
Text Label 5750 2600 1    50   ~ 0
d2
Text Label 5850 2600 1    50   ~ 0
e2
Text Label 5950 2600 1    50   ~ 0
f2
Text Label 6050 2600 1    50   ~ 0
g2
Wire Wire Line
	5450 2600 5450 2450
Wire Wire Line
	5550 2600 5550 2450
Wire Wire Line
	5650 2600 5650 2450
Wire Wire Line
	5750 2600 5750 2450
Wire Wire Line
	5850 2600 5850 2450
Wire Wire Line
	5950 2600 5950 2450
Wire Wire Line
	6050 2600 6050 2450
$Comp
L BA56-12EWA:BA56-12EWA DS3
U 1 1 5FC44F1C
P 5750 2050
F 0 "DS3" H 5300 2700 50  0000 L CNN
F 1 "BA56-12EWA" H 5300 2600 50  0000 L CNN
F 2 "BA56-12EWA(3Digit7Segment):BA56-12EWA" H 5750 2050 50  0001 L BNN
F 3 "" H 5750 2050 50  0001 L BNN
	1    5750 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 3450 6900 3450
Text Label 7100 3450 2    50   ~ 0
DIG7
Wire Wire Line
	5650 3450 5800 3450
Wire Wire Line
	5650 3350 5800 3350
Wire Wire Line
	5650 3250 5800 3250
Wire Wire Line
	5650 3150 5800 3150
Wire Wire Line
	5650 3050 5800 3050
Wire Wire Line
	7300 3350 6900 3350
Wire Wire Line
	7300 3100 7300 3350
$Comp
L power:+5V #PWR0104
U 1 1 5FC44EF5
P 7300 3100
F 0 "#PWR0104" H 7300 2950 50  0001 C CNN
F 1 "+5V" H 7315 3273 50  0000 C CNN
F 2 "" H 7300 3100 50  0001 C CNN
F 3 "" H 7300 3100 50  0001 C CNN
	1    7300 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 2950 5350 3000
Wire Wire Line
	5800 2950 5350 2950
$Comp
L power:GND #PWR0103
U 1 1 5FC44EED
P 5350 3000
F 0 "#PWR0103" H 5350 2750 50  0001 C CNN
F 1 "GND" H 5350 2850 50  0000 C CNN
F 2 "" H 5350 3000 50  0001 C CNN
F 3 "" H 5350 3000 50  0001 C CNN
	1    5350 3000
	1    0    0    -1  
$EndComp
Text Label 5650 3450 0    50   ~ 0
e2
Text Label 5650 3350 0    50   ~ 0
d2
Text Label 5650 3250 0    50   ~ 0
c2
Text Label 5650 3150 0    50   ~ 0
b2
Text Label 5650 3050 0    50   ~ 0
a2
Text Label 6650 2600 1    50   ~ 0
a2
Text Label 6750 2600 1    50   ~ 0
b2
Text Label 6850 2600 1    50   ~ 0
c2
Text Label 6950 2600 1    50   ~ 0
d2
Text Label 7050 2600 1    50   ~ 0
e2
Text Label 7150 2600 1    50   ~ 0
f2
Text Label 7250 2600 1    50   ~ 0
g2
Wire Wire Line
	6650 2600 6650 2450
Wire Wire Line
	6750 2600 6750 2450
Wire Wire Line
	6850 2600 6850 2450
Wire Wire Line
	6950 2600 6950 2450
Wire Wire Line
	7050 2600 7050 2450
Wire Wire Line
	7150 2600 7150 2450
Wire Wire Line
	7250 2600 7250 2450
$Comp
L BA56-12EWA:BA56-12EWA DS4
U 1 1 5FC44ECA
P 6950 2050
F 0 "DS4" H 6550 2700 50  0000 L CNN
F 1 "BA56-12EWA" H 6550 2600 50  0000 L CNN
F 2 "BA56-12EWA(3Digit7Segment):BA56-12EWA" H 6950 2050 50  0001 L BNN
F 3 "" H 6950 2050 50  0001 L BNN
	1    6950 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 3450 4350 3450
Wire Wire Line
	4550 3550 4350 3550
Wire Wire Line
	4550 3650 4350 3650
Wire Wire Line
	4550 3750 4350 3750
Wire Wire Line
	4550 3850 4350 3850
Text Label 4550 3450 2    50   ~ 0
DIG1
Text Label 4550 3550 2    50   ~ 0
DIG2
Text Label 4550 3650 2    50   ~ 0
DIG3
Text Label 4550 3750 2    50   ~ 0
DIG4
Text Label 4550 3850 2    50   ~ 0
DIG5
Wire Wire Line
	4750 3350 4350 3350
Wire Wire Line
	4750 3100 4750 3350
$Comp
L power:+5V #PWR0102
U 1 1 5FC2548C
P 4750 3100
F 0 "#PWR0102" H 4750 2950 50  0001 C CNN
F 1 "+5V" H 4765 3273 50  0000 C CNN
F 2 "" H 4750 3100 50  0001 C CNN
F 3 "" H 4750 3100 50  0001 C CNN
	1    4750 3100
	1    0    0    -1  
$EndComp
Text Label 4200 2600 1    50   ~ 0
b1
Text Label 4300 2600 1    50   ~ 0
c1
Text Label 4400 2600 1    50   ~ 0
d1
Text Label 4500 2600 1    50   ~ 0
e1
Text Label 4600 2600 1    50   ~ 0
f1
Text Label 4700 2600 1    50   ~ 0
g1
Wire Wire Line
	4200 2600 4200 2450
Wire Wire Line
	4300 2600 4300 2450
Wire Wire Line
	4400 2600 4400 2450
Wire Wire Line
	4500 2600 4500 2450
Wire Wire Line
	4600 2600 4600 2450
Wire Wire Line
	4700 2600 4700 2450
$Comp
L DRIVER-TM1636_DIP18_:TM1637 U1
U 1 1 5FC0A678
P 3800 3350
F 0 "U1" H 3800 4015 50  0000 C CNN
F 1 "TM1637" H 3800 3924 50  0000 C CNN
F 2 "Package_SO:SOP-20_7.5x12.8mm_P1.27mm" H 3850 3900 50  0001 C CNN
F 3 "" H 3800 3350 50  0001 C CNN
	1    3800 3350
	1    0    0    -1  
$EndComp
$Comp
L BA56-12EWA:BA56-12EWA DS1
U 1 1 5FB69BF4
P 4400 2050
F 0 "DS1" H 4000 2700 50  0000 L CNN
F 1 "BA56-12EWA" H 4000 2600 50  0000 L CNN
F 2 "BA56-12EWA(3Digit7Segment):BA56-12EWA" H 4400 2050 50  0001 L BNN
F 3 "" H 4400 2050 50  0001 L BNN
	1    4400 2050
	1    0    0    -1  
$EndComp
$EndSCHEMATC
