EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "PMS ISO v0.1"
Date "2021-01-09"
Rev "0"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:C C1
U 1 1 600626EF
P 800 5150
F 0 "C1" H 915 5196 50  0000 L CNN
F 1 "104" H 915 5105 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W3.5mm_P5.00mm" H 838 5000 50  0001 C CNN
F 3 "~" H 800 5150 50  0001 C CNN
	1    800  5150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 5FB0A6B6
P 800 5350
F 0 "#PWR02" H 800 5100 50  0001 C CNN
F 1 "GND" H 805 5177 50  0000 C CNN
F 2 "" H 800 5350 50  0001 C CNN
F 3 "" H 800 5350 50  0001 C CNN
	1    800  5350
	1    0    0    -1  
$EndComp
Wire Wire Line
	800  4900 800  5000
Wire Wire Line
	800  5300 800  5350
Text Notes 700  3950 0    50   ~ 0
MCU
$Comp
L Mechanical:MountingHole H1
U 1 1 60062701
P 10450 5550
F 0 "H1" H 10550 5596 50  0000 L CNN
F 1 "MountingHole" H 10550 5505 50  0000 L CNN
F 2 "MountingHole:MountingHole_3mm_Pad_Via" H 10450 5550 50  0001 C CNN
F 3 "~" H 10450 5550 50  0001 C CNN
	1    10450 5550
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 600722EB
P 10450 5800
F 0 "H2" H 10550 5846 50  0000 L CNN
F 1 "MountingHole" H 10550 5755 50  0000 L CNN
F 2 "MountingHole:MountingHole_3mm_Pad_Via" H 10450 5800 50  0001 C CNN
F 3 "~" H 10450 5800 50  0001 C CNN
	1    10450 5800
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 60062703
P 10450 6050
F 0 "H3" H 10550 6096 50  0000 L CNN
F 1 "MountingHole" H 10550 6005 50  0000 L CNN
F 2 "MountingHole:MountingHole_3mm_Pad_Via" H 10450 6050 50  0001 C CNN
F 3 "~" H 10450 6050 50  0001 C CNN
	1    10450 6050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 60072588
P 10450 6300
F 0 "H4" H 10550 6346 50  0000 L CNN
F 1 "MountingHole" H 10550 6255 50  0000 L CNN
F 2 "MountingHole:MountingHole_3mm_Pad_Via" H 10450 6300 50  0001 C CNN
F 3 "~" H 10450 6300 50  0001 C CNN
	1    10450 6300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR08
U 1 1 5FEB5C8B
P 1250 5300
F 0 "#PWR08" H 1250 5050 50  0001 C CNN
F 1 "GND" H 1255 5127 50  0000 C CNN
F 2 "" H 1250 5300 50  0001 C CNN
F 3 "" H 1250 5300 50  0001 C CNN
	1    1250 5300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1250 5300 1250 5200
$Comp
L power:GND #PWR013
U 1 1 600626F4
P 3700 4450
F 0 "#PWR013" H 3700 4200 50  0001 C CNN
F 1 "GND" H 3705 4277 50  0000 C CNN
F 2 "" H 3700 4450 50  0001 C CNN
F 3 "" H 3700 4450 50  0001 C CNN
	1    3700 4450
	1    0    0    -1  
$EndComp
Text Label 3550 4200 0    50   ~ 0
RXD
Text Label 3550 4300 0    50   ~ 0
TXD
$Comp
L Device:R R2
U 1 1 5FEE94B5
P 1600 4500
F 0 "R2" H 1700 4400 50  0000 L CNN
F 1 "10k" H 1650 4500 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 1530 4500 50  0001 C CNN
F 3 "~" H 1600 4500 50  0001 C CNN
	1    1600 4500
	-1   0    0    1   
$EndComp
$Comp
L Device:R R3
U 1 1 5FEEA206
P 1850 4500
F 0 "R3" H 1950 4400 50  0000 L CNN
F 1 "10k" H 1900 4500 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 1780 4500 50  0001 C CNN
F 3 "~" H 1850 4500 50  0001 C CNN
	1    1850 4500
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR025
U 1 1 5FEEA735
P 2500 6150
F 0 "#PWR025" H 2500 5900 50  0001 C CNN
F 1 "GND" H 2505 5977 50  0000 C CNN
F 2 "" H 2500 6150 50  0001 C CNN
F 3 "" H 2500 6150 50  0001 C CNN
	1    2500 6150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 6150 2500 6050
$Comp
L power:+3.3V #PWR023
U 1 1 5FEEE3E8
P 2500 4200
F 0 "#PWR023" H 2500 4050 50  0001 C CNN
F 1 "+3.3V" H 2515 4373 50  0000 C CNN
F 2 "" H 2500 4200 50  0001 C CNN
F 3 "" H 2500 4200 50  0001 C CNN
	1    2500 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 4650 1850 4750
Wire Wire Line
	1850 4750 1900 4750
Wire Wire Line
	1600 4650 1600 4950
Wire Wire Line
	1600 4950 1900 4950
$Comp
L power:+3.3V #PWR016
U 1 1 5FEFEF6B
P 1850 4200
F 0 "#PWR016" H 1850 4050 50  0001 C CNN
F 1 "+3.3V" H 1865 4373 50  0000 C CNN
F 2 "" H 1850 4200 50  0001 C CNN
F 3 "" H 1850 4200 50  0001 C CNN
	1    1850 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 4350 1850 4250
Wire Wire Line
	1600 4350 1600 4250
Wire Wire Line
	1600 4250 1850 4250
Connection ~ 1850 4250
Wire Wire Line
	1850 4250 1850 4200
Wire Wire Line
	2500 4200 2500 4250
$Comp
L Regulator_Linear:AMS1117-3.3 U1
U 1 1 5FF50324
P 2300 2900
F 0 "U1" H 2300 3142 50  0000 C CNN
F 1 "AMS1117-3.3" H 2300 3051 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-223-3_TabPin2" H 2300 3100 50  0001 C CNN
F 3 "http://www.advanced-monolithic.com/pdf/ds1117.pdf" H 2400 2650 50  0001 C CNN
	1    2300 2900
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C13
U 1 1 5FF5739D
P 2750 3150
F 0 "C13" H 2868 3196 50  0000 L CNN
F 1 "10uF" H 2868 3105 50  0000 L CNN
F 2 "Capacitor_THT:C_Radial_D6.3mm_H11.0mm_P2.50mm" H 2788 3000 50  0001 C CNN
F 3 "~" H 2750 3150 50  0001 C CNN
	1    2750 3150
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR04
U 1 1 5FF578C6
P 800 2850
F 0 "#PWR04" H 800 2700 50  0001 C CNN
F 1 "+5V" H 815 3023 50  0000 C CNN
F 2 "" H 800 2850 50  0001 C CNN
F 3 "" H 800 2850 50  0001 C CNN
	1    800  2850
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR028
U 1 1 5FF584A5
P 3000 2850
F 0 "#PWR028" H 3000 2700 50  0001 C CNN
F 1 "+3.3V" H 3015 3023 50  0000 C CNN
F 2 "" H 3000 2850 50  0001 C CNN
F 3 "" H 3000 2850 50  0001 C CNN
	1    3000 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 2900 2750 2900
Wire Wire Line
	3000 2900 3000 2850
Wire Wire Line
	2750 3000 2750 2900
Connection ~ 2750 2900
Wire Wire Line
	2750 2900 3000 2900
$Comp
L power:GND #PWR021
U 1 1 5FF68240
P 2300 3400
F 0 "#PWR021" H 2300 3150 50  0001 C CNN
F 1 "GND" H 2305 3227 50  0000 C CNN
F 2 "" H 2300 3400 50  0001 C CNN
F 3 "" H 2300 3400 50  0001 C CNN
	1    2300 3400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR027
U 1 1 5FF6851C
P 2750 3400
F 0 "#PWR027" H 2750 3150 50  0001 C CNN
F 1 "GND" H 2755 3227 50  0000 C CNN
F 2 "" H 2750 3400 50  0001 C CNN
F 3 "" H 2750 3400 50  0001 C CNN
	1    2750 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 3400 2300 3200
Wire Wire Line
	2750 3400 2750 3300
Wire Wire Line
	800  2900 800  2850
Wire Wire Line
	950  3000 950  2900
Connection ~ 950  2900
Wire Wire Line
	950  2900 800  2900
Wire Wire Line
	1400 3000 1400 2900
Connection ~ 1400 2900
Wire Wire Line
	1400 2900 950  2900
Wire Wire Line
	1800 3000 1800 2900
Wire Wire Line
	1800 2900 1400 2900
$Comp
L power:GND #PWR015
U 1 1 5FFA7244
P 1800 3400
F 0 "#PWR015" H 1800 3150 50  0001 C CNN
F 1 "GND" H 1805 3227 50  0000 C CNN
F 2 "" H 1800 3400 50  0001 C CNN
F 3 "" H 1800 3400 50  0001 C CNN
	1    1800 3400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR010
U 1 1 5FFA744B
P 1400 3400
F 0 "#PWR010" H 1400 3150 50  0001 C CNN
F 1 "GND" H 1405 3227 50  0000 C CNN
F 2 "" H 1400 3400 50  0001 C CNN
F 3 "" H 1400 3400 50  0001 C CNN
	1    1400 3400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR05
U 1 1 5FFA75F4
P 950 3400
F 0 "#PWR05" H 950 3150 50  0001 C CNN
F 1 "GND" H 955 3227 50  0000 C CNN
F 2 "" H 950 3400 50  0001 C CNN
F 3 "" H 950 3400 50  0001 C CNN
	1    950  3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	950  3400 950  3300
Wire Wire Line
	1400 3400 1400 3300
Wire Wire Line
	1800 3400 1800 3300
$Comp
L Device:CP C4
U 1 1 5FFBDF70
P 950 3150
F 0 "C4" H 1068 3196 50  0000 L CNN
F 1 "10uF" H 1068 3105 50  0000 L CNN
F 2 "Capacitor_THT:C_Radial_D6.3mm_H11.0mm_P2.50mm" H 988 3000 50  0001 C CNN
F 3 "~" H 950 3150 50  0001 C CNN
	1    950  3150
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C10
U 1 1 5FFBDF76
P 1400 3150
F 0 "C10" H 1518 3196 50  0000 L CNN
F 1 "1uF" H 1518 3105 50  0000 L CNN
F 2 "Capacitor_THT:C_Radial_D6.3mm_H11.0mm_P2.50mm" H 1438 3000 50  0001 C CNN
F 3 "~" H 1400 3150 50  0001 C CNN
	1    1400 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C12
U 1 1 5FFBDF7C
P 1800 3150
F 0 "C12" H 1915 3196 50  0000 L CNN
F 1 "104" H 1915 3105 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W3.5mm_P5.00mm" H 1838 3000 50  0001 C CNN
F 3 "~" H 1800 3150 50  0001 C CNN
	1    1800 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 2900 1800 2900
Connection ~ 1800 2900
$Comp
L Switch:SW_Push SW1
U 1 1 5FEA2066
P 1250 5000
F 0 "SW1" V 1296 4952 50  0000 R CNN
F 1 "RST" V 1205 4952 50  0000 R CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 1250 5200 50  0001 C CNN
F 3 "~" H 1250 5200 50  0001 C CNN
	1    1250 5000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1850 4750 1250 4750
Wire Wire Line
	1250 4750 1250 4800
Connection ~ 1850 4750
$Comp
L Device:R R10
U 1 1 600B274A
P 3400 5750
F 0 "R10" V 3350 5500 50  0000 L CNN
F 1 "10k" V 3400 5700 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 3330 5750 50  0001 C CNN
F 3 "~" H 3400 5750 50  0001 C CNN
	1    3400 5750
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR035
U 1 1 600B2750
P 3950 5900
F 0 "#PWR035" H 3950 5650 50  0001 C CNN
F 1 "GND" H 3955 5727 50  0000 C CNN
F 2 "" H 3950 5900 50  0001 C CNN
F 3 "" H 3950 5900 50  0001 C CNN
	1    3950 5900
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D5
U 1 1 600B2756
P 3750 5750
F 0 "D5" H 3950 5700 50  0000 R CNN
F 1 "LED" H 3800 5850 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 3750 5750 50  0001 C CNN
F 3 "~" H 3750 5750 50  0001 C CNN
	1    3750 5750
	-1   0    0    1   
$EndComp
Wire Wire Line
	3600 5750 3550 5750
Text Label 3350 4850 2    50   ~ 0
TXD
Wire Wire Line
	3350 4850 3100 4850
Text Label 3350 5050 2    50   ~ 0
RXD
Wire Wire Line
	3350 5050 3100 5050
$Comp
L Device:R R5
U 1 1 6010D408
P 3300 4500
F 0 "R5" H 3400 4400 50  0000 L CNN
F 1 "10k" H 3350 4500 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 3230 4500 50  0001 C CNN
F 3 "~" H 3300 4500 50  0001 C CNN
	1    3300 4500
	-1   0    0    1   
$EndComp
Wire Wire Line
	3100 4750 3300 4750
Wire Wire Line
	3300 4750 3300 4650
Wire Wire Line
	3300 4350 3300 4250
Wire Wire Line
	3300 4250 2500 4250
Connection ~ 2500 4250
Wire Wire Line
	2500 4250 2500 4550
$Comp
L Device:R R6
U 1 1 6011D8D0
P 3400 5650
F 0 "R6" V 3350 5400 50  0000 L CNN
F 1 "10k" V 3400 5600 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 3330 5650 50  0001 C CNN
F 3 "~" H 3400 5650 50  0001 C CNN
	1    3400 5650
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR036
U 1 1 601260D7
P 4100 5900
F 0 "#PWR036" H 4100 5650 50  0001 C CNN
F 1 "GND" H 4105 5727 50  0000 C CNN
F 2 "" H 4100 5900 50  0001 C CNN
F 3 "" H 4100 5900 50  0001 C CNN
	1    4100 5900
	1    0    0    -1  
$EndComp
Text Label 3350 5150 2    50   ~ 0
STX
$Comp
L Switch:SW_Push SW2
U 1 1 601440B4
P 3800 5000
F 0 "SW2" V 3846 4952 50  0000 R CNN
F 1 "PG" V 3755 4952 50  0000 R CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 3800 5200 50  0001 C CNN
F 3 "~" H 3800 5200 50  0001 C CNN
	1    3800 5000
	0    1    1    0   
$EndComp
Wire Wire Line
	3300 4750 3800 4750
Wire Wire Line
	3800 4750 3800 4800
Connection ~ 3300 4750
$Comp
L power:GND #PWR033
U 1 1 601596F6
P 3800 5300
F 0 "#PWR033" H 3800 5050 50  0001 C CNN
F 1 "GND" H 3805 5127 50  0000 C CNN
F 2 "" H 3800 5300 50  0001 C CNN
F 3 "" H 3800 5300 50  0001 C CNN
	1    3800 5300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 5300 3800 5200
Wire Wire Line
	3350 5150 3100 5150
$Comp
L Device:C C11
U 1 1 601DA68B
P 9250 2250
F 0 "C11" H 9365 2296 50  0000 L CNN
F 1 "104" H 9365 2205 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W3.5mm_P5.00mm" H 9288 2100 50  0001 C CNN
F 3 "~" H 9250 2250 50  0001 C CNN
	1    9250 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	9250 2100 9250 2000
Wire Wire Line
	9250 2400 9250 2500
$Comp
L Device:CP C7
U 1 1 602B0977
P 8850 2250
F 0 "C7" H 8968 2296 50  0000 L CNN
F 1 "10uF" H 8968 2205 50  0000 L CNN
F 2 "Capacitor_THT:C_Radial_D6.3mm_H11.0mm_P2.50mm" H 8888 2100 50  0001 C CNN
F 3 "~" H 8850 2250 50  0001 C CNN
	1    8850 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	8850 2500 8850 2400
Wire Wire Line
	8850 2100 8850 2000
Wire Wire Line
	3950 5900 3950 5750
Wire Wire Line
	3950 5750 3900 5750
Wire Wire Line
	4100 5900 4100 5650
Wire Wire Line
	3550 5650 4100 5650
Wire Wire Line
	3250 5650 3100 5650
Wire Wire Line
	3250 5750 3100 5750
Wire Wire Line
	3550 4200 3800 4200
Wire Wire Line
	3550 4300 3800 4300
Wire Wire Line
	3800 4400 3700 4400
Wire Wire Line
	3700 4400 3700 4450
$Comp
L Connector_Generic:Conn_01x04 J4
U 1 1 6040BD87
P 6500 4200
F 0 "J4" H 6580 4192 50  0000 L CNN
F 1 "TFT" H 6580 4101 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 6500 4200 50  0001 C CNN
F 3 "~" H 6500 4200 50  0001 C CNN
	1    6500 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6300 4400 6200 4400
Wire Wire Line
	6200 4400 6200 4450
$Comp
L My_Lib_KiCAD:ESP12E_My U4
U 1 1 606FB5DD
P 2500 5250
F 0 "U4" H 2150 6050 50  0000 C CNN
F 1 "ESP12E_My" H 2250 5950 50  0000 C CNN
F 2 "RF_Module:ESP-12E" H 2050 6050 50  0001 C CNN
F 3 "" H 3500 4450 50  0001 C CNN
	1    2500 5250
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0101
U 1 1 608FF5A0
P 800 4900
F 0 "#PWR0101" H 800 4750 50  0001 C CNN
F 1 "+3.3V" H 815 5073 50  0000 C CNN
F 2 "" H 800 4900 50  0001 C CNN
F 3 "" H 800 4900 50  0001 C CNN
	1    800  4900
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J3
U 1 1 60943597
P 4000 4300
F 0 "J3" H 3850 3950 50  0000 L CNN
F 1 "Program" H 3850 4050 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 4000 4300 50  0001 C CNN
F 3 "~" H 4000 4300 50  0001 C CNN
	1    4000 4300
	1    0    0    1   
$EndComp
$Comp
L Converter_ACDC:HLK-PM01 PS1
U 1 1 5FFB1CD6
P 3250 1050
F 0 "PS1" H 3250 1375 50  0000 C CNN
F 1 "HLK-PM01" H 3250 1284 50  0000 C CNN
F 2 "Converter_ACDC:Converter_ACDC_HiLink_HLK-PMxx" H 3250 750 50  0001 C CNN
F 3 "http://www.hlktech.net/product_detail.php?ProId=54" H 3650 700 50  0001 C CNN
	1    3250 1050
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J7
U 1 1 5FFC8ED1
P 800 1150
F 0 "J7" H 850 850 50  0000 C CNN
F 1 "PowerIN" H 750 950 50  0000 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 800 1150 50  0001 C CNN
F 3 "~" H 800 1150 50  0001 C CNN
	1    800  1150
	-1   0    0    1   
$EndComp
Wire Wire Line
	1050 1050 1000 1050
Wire Wire Line
	1050 1150 1000 1150
$Comp
L Device:CP C14
U 1 1 5FFEC8EE
P 3900 1150
F 0 "C14" H 3950 1250 50  0000 L CNN
F 1 "220uF" H 3900 1050 50  0000 L CNN
F 2 "Capacitor_THT:C_Radial_D6.3mm_H11.0mm_P2.50mm" H 3938 1000 50  0001 C CNN
F 3 "~" H 3900 1150 50  0001 C CNN
	1    3900 1150
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C16
U 1 1 5FFEC8F4
P 4250 1150
F 0 "C16" H 4368 1196 50  0000 L CNN
F 1 "10uF" H 4368 1105 50  0000 L CNN
F 2 "Capacitor_THT:C_Radial_D6.3mm_H11.0mm_P2.50mm" H 4288 1000 50  0001 C CNN
F 3 "~" H 4250 1150 50  0001 C CNN
	1    4250 1150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C18
U 1 1 5FFEC8FA
P 4650 1150
F 0 "C18" H 4765 1196 50  0000 L CNN
F 1 "104" H 4765 1105 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W3.5mm_P5.00mm" H 4688 1000 50  0001 C CNN
F 3 "~" H 4650 1150 50  0001 C CNN
	1    4650 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 1400 3900 1300
Wire Wire Line
	4250 1400 4250 1300
Wire Wire Line
	4650 1400 4650 1300
Wire Wire Line
	3650 950  3900 950 
Wire Wire Line
	4900 950  4900 900 
Wire Wire Line
	3900 1000 3900 950 
Connection ~ 3900 950 
Wire Wire Line
	3900 950  4250 950 
Wire Wire Line
	4250 1000 4250 950 
Connection ~ 4250 950 
Wire Wire Line
	4650 1000 4650 950 
Connection ~ 4650 950 
Wire Wire Line
	4650 950  4900 950 
Wire Wire Line
	3650 1150 3700 1150
Wire Wire Line
	3700 1150 3700 1400
$Comp
L power:+5VA #PWR052
U 1 1 60054097
P 4900 900
F 0 "#PWR052" H 4900 750 50  0001 C CNN
F 1 "+5VA" H 4915 1073 50  0000 C CNN
F 2 "" H 4900 900 50  0001 C CNN
F 3 "" H 4900 900 50  0001 C CNN
	1    4900 900 
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR01
U 1 1 60054F79
P 3700 1400
F 0 "#PWR01" H 3700 1150 50  0001 C CNN
F 1 "GNDA" H 3705 1227 50  0000 C CNN
F 2 "" H 3700 1400 50  0001 C CNN
F 3 "" H 3700 1400 50  0001 C CNN
	1    3700 1400
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR037
U 1 1 60055E53
P 3900 1400
F 0 "#PWR037" H 3900 1150 50  0001 C CNN
F 1 "GNDA" H 3905 1227 50  0000 C CNN
F 2 "" H 3900 1400 50  0001 C CNN
F 3 "" H 3900 1400 50  0001 C CNN
	1    3900 1400
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR048
U 1 1 60056017
P 4250 1400
F 0 "#PWR048" H 4250 1150 50  0001 C CNN
F 1 "GNDA" H 4255 1227 50  0000 C CNN
F 2 "" H 4250 1400 50  0001 C CNN
F 3 "" H 4250 1400 50  0001 C CNN
	1    4250 1400
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR050
U 1 1 60056216
P 4650 1400
F 0 "#PWR050" H 4650 1150 50  0001 C CNN
F 1 "GNDA" H 4655 1227 50  0000 C CNN
F 2 "" H 4650 1400 50  0001 C CNN
F 3 "" H 4650 1400 50  0001 C CNN
	1    4650 1400
	1    0    0    -1  
$EndComp
$Comp
L Converter_ACDC:HLK-PM01 PS2
U 1 1 6008BAF3
P 3250 2200
F 0 "PS2" H 3250 2525 50  0000 C CNN
F 1 "HLK-PM01" H 3250 2434 50  0000 C CNN
F 2 "Converter_ACDC:Converter_ACDC_HiLink_HLK-PMxx" H 3250 1900 50  0001 C CNN
F 3 "http://www.hlktech.net/product_detail.php?ProId=54" H 3650 1850 50  0001 C CNN
	1    3250 2200
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J8
U 1 1 6008BAF9
P 800 2250
F 0 "J8" H 850 1950 50  0000 C CNN
F 1 "PowerIN" H 750 2050 50  0000 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 800 2250 50  0001 C CNN
F 3 "~" H 800 2250 50  0001 C CNN
	1    800  2250
	-1   0    0    1   
$EndComp
Wire Wire Line
	2850 2100 2700 2100
Wire Wire Line
	1050 2150 1000 2150
Wire Wire Line
	2850 2300 2700 2300
Wire Wire Line
	1050 2250 1000 2250
Wire Wire Line
	3650 2300 3700 2300
Wire Wire Line
	3700 2300 3700 2550
$Comp
L power:+5V #PWR053
U 1 1 600A0D14
P 4150 2050
F 0 "#PWR053" H 4150 1900 50  0001 C CNN
F 1 "+5V" H 4165 2223 50  0000 C CNN
F 2 "" H 4150 2050 50  0001 C CNN
F 3 "" H 4150 2050 50  0001 C CNN
	1    4150 2050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR012
U 1 1 600A155A
P 3700 2550
F 0 "#PWR012" H 3700 2300 50  0001 C CNN
F 1 "GND" H 3705 2377 50  0000 C CNN
F 2 "" H 3700 2550 50  0001 C CNN
F 3 "" H 3700 2550 50  0001 C CNN
	1    3700 2550
	1    0    0    -1  
$EndComp
$Comp
L Interface_UART:MAX485E U9
U 1 1 600F0FB5
P 7800 1450
F 0 "U9" H 7400 2000 50  0000 C CNN
F 1 "MAX485E" H 7500 1900 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 7800 750 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX1487E-MAX491E.pdf" H 7800 1500 50  0001 C CNN
	1    7800 1450
	1    0    0    -1  
$EndComp
$Comp
L Isolator:PC817 U7
U 1 1 600F3191
P 5900 3100
F 0 "U7" H 5900 3425 50  0000 C CNN
F 1 "PC817" H 5900 3334 50  0000 C CNN
F 2 "Package_DIP:DIP-4_W7.62mm" H 5700 2900 50  0001 L CIN
F 3 "http://www.soselectronic.cz/a_info/resource/d/pc817.pdf" H 5900 3100 50  0001 L CNN
	1    5900 3100
	1    0    0    -1  
$EndComp
Text Label 5200 3200 0    50   ~ 0
TXD
Wire Wire Line
	5200 3200 5600 3200
$Comp
L Device:R R21
U 1 1 60123C96
P 5500 2550
F 0 "R21" H 5600 2450 50  0000 L CNN
F 1 "120R" H 5550 2550 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 5430 2550 50  0001 C CNN
F 3 "~" H 5500 2550 50  0001 C CNN
	1    5500 2550
	-1   0    0    1   
$EndComp
Wire Wire Line
	5600 3000 5500 3000
$Comp
L power:+3.3V #PWR054
U 1 1 60138E22
P 5500 2300
F 0 "#PWR054" H 5500 2150 50  0001 C CNN
F 1 "+3.3V" H 5515 2473 50  0000 C CNN
F 2 "" H 5500 2300 50  0001 C CNN
F 3 "" H 5500 2300 50  0001 C CNN
	1    5500 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 2400 5500 2300
$Comp
L power:GNDA #PWR055
U 1 1 6014F9C4
P 6300 3350
F 0 "#PWR055" H 6300 3100 50  0001 C CNN
F 1 "GNDA" H 6305 3177 50  0000 C CNN
F 2 "" H 6300 3350 50  0001 C CNN
F 3 "" H 6300 3350 50  0001 C CNN
	1    6300 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 3200 6300 3200
Wire Wire Line
	6300 3200 6300 3350
$Comp
L Device:R R24
U 1 1 6016565F
P 6450 2550
F 0 "R24" H 6550 2450 50  0000 L CNN
F 1 "1k" H 6500 2550 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 6380 2550 50  0001 C CNN
F 3 "~" H 6450 2550 50  0001 C CNN
	1    6450 2550
	-1   0    0    1   
$EndComp
$Comp
L Device:R R25
U 1 1 60165AAE
P 6750 3000
F 0 "R25" V 6950 3000 50  0000 L CNN
F 1 "10k" V 6850 2950 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 6680 3000 50  0001 C CNN
F 3 "~" H 6750 3000 50  0001 C CNN
	1    6750 3000
	0    -1   -1   0   
$EndComp
Connection ~ 6450 3000
Wire Wire Line
	6450 3000 6600 3000
$Comp
L power:+5VA #PWR057
U 1 1 601913C4
P 6450 2300
F 0 "#PWR057" H 6450 2150 50  0001 C CNN
F 1 "+5VA" H 6465 2473 50  0000 C CNN
F 2 "" H 6450 2300 50  0001 C CNN
F 3 "" H 6450 2300 50  0001 C CNN
	1    6450 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	6450 2300 6450 2400
Wire Wire Line
	7000 3000 6900 3000
$Comp
L power:GNDA #PWR060
U 1 1 601BEE04
P 7300 3350
F 0 "#PWR060" H 7300 3100 50  0001 C CNN
F 1 "GNDA" H 7305 3177 50  0000 C CNN
F 2 "" H 7300 3350 50  0001 C CNN
F 3 "" H 7300 3350 50  0001 C CNN
	1    7300 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	7300 3350 7300 3200
Wire Wire Line
	6200 3000 6450 3000
Text Label 6750 3250 2    50   ~ 0
DI
Wire Wire Line
	6750 3250 6450 3250
Wire Wire Line
	6450 3250 6450 3000
$Comp
L Device:R R26
U 1 1 6022E61E
P 7300 2550
F 0 "R26" H 7400 2450 50  0000 L CNN
F 1 "1k" H 7350 2550 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 7230 2550 50  0001 C CNN
F 3 "~" H 7300 2550 50  0001 C CNN
	1    7300 2550
	-1   0    0    1   
$EndComp
Wire Wire Line
	7300 2800 7300 2750
$Comp
L power:+5VA #PWR059
U 1 1 60244D75
P 7300 2300
F 0 "#PWR059" H 7300 2150 50  0001 C CNN
F 1 "+5VA" H 7315 2473 50  0000 C CNN
F 2 "" H 7300 2300 50  0001 C CNN
F 3 "" H 7300 2300 50  0001 C CNN
	1    7300 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	7300 2400 7300 2300
Wire Wire Line
	6450 2700 6450 3000
Wire Wire Line
	5500 2700 5500 3000
Text Label 7600 2750 2    50   ~ 0
DE,RE
Wire Wire Line
	7600 2750 7300 2750
Connection ~ 7300 2750
Wire Wire Line
	7300 2750 7300 2700
Text Label 7050 1450 0    50   ~ 0
DE,RE
Wire Wire Line
	7400 1450 7300 1450
Wire Wire Line
	7400 1550 7300 1550
Wire Wire Line
	7300 1550 7300 1450
Connection ~ 7300 1450
Wire Wire Line
	7300 1450 7050 1450
$Comp
L power:GNDA #PWR063
U 1 1 60312CD1
P 7800 2150
F 0 "#PWR063" H 7800 1900 50  0001 C CNN
F 1 "GNDA" H 7805 1977 50  0000 C CNN
F 2 "" H 7800 2150 50  0001 C CNN
F 3 "" H 7800 2150 50  0001 C CNN
	1    7800 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7800 2150 7800 2050
$Comp
L power:+5VA #PWR062
U 1 1 6032A577
P 7800 800
F 0 "#PWR062" H 7800 650 50  0001 C CNN
F 1 "+5VA" H 7815 973 50  0000 C CNN
F 2 "" H 7800 800 50  0001 C CNN
F 3 "" H 7800 800 50  0001 C CNN
	1    7800 800 
	1    0    0    -1  
$EndComp
Wire Wire Line
	7800 950  7800 800 
Text Label 7050 1650 0    50   ~ 0
DI
Wire Wire Line
	7050 1650 7400 1650
Text Label 7050 1350 0    50   ~ 0
RO
Wire Wire Line
	7050 1350 7400 1350
$Comp
L Isolator:PC817 U8
U 1 1 603BE359
P 5950 1550
F 0 "U8" H 5950 1875 50  0000 C CNN
F 1 "PC817" H 5950 1784 50  0000 C CNN
F 2 "Package_DIP:DIP-4_W7.62mm" H 5750 1350 50  0001 L CIN
F 3 "http://www.soselectronic.cz/a_info/resource/d/pc817.pdf" H 5950 1550 50  0001 L CNN
	1    5950 1550
	-1   0    0    -1  
$EndComp
Text Label 6550 1650 2    50   ~ 0
RO
Wire Wire Line
	6550 1650 6250 1650
$Comp
L Device:R R27
U 1 1 603D713C
P 6400 1200
F 0 "R27" H 6500 1100 50  0000 L CNN
F 1 "330R" H 6450 1200 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 6330 1200 50  0001 C CNN
F 3 "~" H 6400 1200 50  0001 C CNN
	1    6400 1200
	-1   0    0    1   
$EndComp
Wire Wire Line
	6250 1450 6400 1450
Wire Wire Line
	6400 1450 6400 1350
$Comp
L power:+5VA #PWR061
U 1 1 603F0247
P 6400 950
F 0 "#PWR061" H 6400 800 50  0001 C CNN
F 1 "+5VA" H 6415 1123 50  0000 C CNN
F 2 "" H 6400 950 50  0001 C CNN
F 3 "" H 6400 950 50  0001 C CNN
	1    6400 950 
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 1050 6400 950 
$Comp
L power:GND #PWR058
U 1 1 60409332
P 5550 1750
F 0 "#PWR058" H 5550 1500 50  0001 C CNN
F 1 "GND" H 5555 1577 50  0000 C CNN
F 2 "" H 5550 1750 50  0001 C CNN
F 3 "" H 5550 1750 50  0001 C CNN
	1    5550 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 1750 5550 1650
Wire Wire Line
	5550 1650 5650 1650
Text Label 5200 1450 0    50   ~ 0
RXD
Wire Wire Line
	5650 1450 5500 1450
$Comp
L Device:R R22
U 1 1 6043B508
P 5500 1200
F 0 "R22" H 5600 1100 50  0000 L CNN
F 1 "470R" H 5550 1200 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 5430 1200 50  0001 C CNN
F 3 "~" H 5500 1200 50  0001 C CNN
	1    5500 1200
	-1   0    0    1   
$EndComp
Wire Wire Line
	5500 1350 5500 1450
Connection ~ 5500 1450
Wire Wire Line
	5500 1450 5200 1450
$Comp
L power:+3.3V #PWR056
U 1 1 60454B9A
P 5500 950
F 0 "#PWR056" H 5500 800 50  0001 C CNN
F 1 "+3.3V" H 5515 1123 50  0000 C CNN
F 2 "" H 5500 950 50  0001 C CNN
F 3 "" H 5500 950 50  0001 C CNN
	1    5500 950 
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 1050 5500 950 
$Comp
L Connector:Screw_Terminal_01x02 J9
U 1 1 6046F2D7
P 9300 1450
F 0 "J9" H 9250 1700 50  0000 C CNN
F 1 "Mobus_OUT" H 9250 1600 50  0000 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 9300 1450 50  0001 C CNN
F 3 "~" H 9300 1450 50  0001 C CNN
	1    9300 1450
	1    0    0    -1  
$EndComp
$Comp
L power:+5VA #PWR06
U 1 1 604A6C47
P 8400 800
F 0 "#PWR06" H 8400 650 50  0001 C CNN
F 1 "+5VA" H 8415 973 50  0000 C CNN
F 2 "" H 8400 800 50  0001 C CNN
F 3 "" H 8400 800 50  0001 C CNN
	1    8400 800 
	1    0    0    -1  
$EndComp
$Comp
L Device:R R28
U 1 1 604A62E9
P 8400 1050
F 0 "R28" H 8500 950 50  0000 L CNN
F 1 "20k" H 8450 1050 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 8330 1050 50  0001 C CNN
F 3 "~" H 8400 1050 50  0001 C CNN
	1    8400 1050
	-1   0    0    1   
$EndComp
Wire Wire Line
	8400 900  8400 800 
$Comp
L Device:R R29
U 1 1 604F489D
P 8500 1900
F 0 "R29" H 8600 1800 50  0000 L CNN
F 1 "20k" H 8550 1900 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 8430 1900 50  0001 C CNN
F 3 "~" H 8500 1900 50  0001 C CNN
	1    8500 1900
	-1   0    0    1   
$EndComp
$Comp
L Device:R R30
U 1 1 604A58F5
P 8900 1500
F 0 "R30" H 9000 1400 50  0000 L CNN
F 1 "120R" H 8950 1500 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 8830 1500 50  0001 C CNN
F 3 "~" H 8900 1500 50  0001 C CNN
	1    8900 1500
	-1   0    0    1   
$EndComp
Wire Wire Line
	8400 1200 8400 1650
Wire Wire Line
	8400 1650 8200 1650
Wire Wire Line
	8200 1350 8500 1350
Wire Wire Line
	8500 1350 8500 1750
Wire Wire Line
	8900 1350 8500 1350
Connection ~ 8500 1350
Wire Wire Line
	8900 1650 8400 1650
Connection ~ 8400 1650
Wire Wire Line
	8900 1350 9100 1350
Wire Wire Line
	9100 1350 9100 1450
Connection ~ 8900 1350
Wire Wire Line
	9100 1550 9100 1650
Wire Wire Line
	9100 1650 8900 1650
Connection ~ 8900 1650
$Comp
L power:GNDA #PWR011
U 1 1 605C62AB
P 8500 2150
F 0 "#PWR011" H 8500 1900 50  0001 C CNN
F 1 "GNDA" H 8505 1977 50  0000 C CNN
F 2 "" H 8500 2150 50  0001 C CNN
F 3 "" H 8500 2150 50  0001 C CNN
	1    8500 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	8500 2150 8500 2050
$Comp
L power:+5VA #PWR064
U 1 1 60601B3D
P 8850 2000
F 0 "#PWR064" H 8850 1850 50  0001 C CNN
F 1 "+5VA" H 8865 2173 50  0000 C CNN
F 2 "" H 8850 2000 50  0001 C CNN
F 3 "" H 8850 2000 50  0001 C CNN
	1    8850 2000
	1    0    0    -1  
$EndComp
$Comp
L power:+5VA #PWR065
U 1 1 60601F9B
P 9250 2000
F 0 "#PWR065" H 9250 1850 50  0001 C CNN
F 1 "+5VA" H 9265 2173 50  0000 C CNN
F 2 "" H 9250 2000 50  0001 C CNN
F 3 "" H 9250 2000 50  0001 C CNN
	1    9250 2000
	1    0    0    -1  
$EndComp
$Comp
L power:+5VA #PWR018
U 1 1 6068F222
P 5900 4050
F 0 "#PWR018" H 5900 3900 50  0001 C CNN
F 1 "+5VA" H 5915 4223 50  0000 C CNN
F 2 "" H 5900 4050 50  0001 C CNN
F 3 "" H 5900 4050 50  0001 C CNN
	1    5900 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 4050 5900 4300
$Comp
L Isolator:PC817 U2
U 1 1 606A40FA
P 5050 4850
F 0 "U2" H 5050 5175 50  0000 C CNN
F 1 "PC817" H 5050 5084 50  0000 C CNN
F 2 "Package_DIP:DIP-4_W7.62mm" H 4850 4650 50  0001 L CIN
F 3 "http://www.soselectronic.cz/a_info/resource/d/pc817.pdf" H 5050 4850 50  0001 L CNN
	1    5050 4850
	1    0    0    -1  
$EndComp
Text Label 4350 4950 0    50   ~ 0
STX
Wire Wire Line
	4350 4950 4750 4950
$Comp
L Device:R R1
U 1 1 606A4102
P 4650 4300
F 0 "R1" H 4750 4200 50  0000 L CNN
F 1 "120R" H 4700 4300 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 4580 4300 50  0001 C CNN
F 3 "~" H 4650 4300 50  0001 C CNN
	1    4650 4300
	-1   0    0    1   
$EndComp
Wire Wire Line
	4750 4750 4650 4750
$Comp
L power:+3.3V #PWR03
U 1 1 606A4109
P 4650 4050
F 0 "#PWR03" H 4650 3900 50  0001 C CNN
F 1 "+3.3V" H 4665 4223 50  0000 C CNN
F 2 "" H 4650 4050 50  0001 C CNN
F 3 "" H 4650 4050 50  0001 C CNN
	1    4650 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 4150 4650 4050
$Comp
L power:GNDA #PWR09
U 1 1 606A4110
P 5450 5100
F 0 "#PWR09" H 5450 4850 50  0001 C CNN
F 1 "GNDA" H 5455 4927 50  0000 C CNN
F 2 "" H 5450 5100 50  0001 C CNN
F 3 "" H 5450 5100 50  0001 C CNN
	1    5450 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 4950 5450 4950
Wire Wire Line
	5450 4950 5450 5100
$Comp
L Device:R R4
U 1 1 606A4118
P 5600 4300
F 0 "R4" H 5700 4200 50  0000 L CNN
F 1 "1k" H 5650 4300 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 5530 4300 50  0001 C CNN
F 3 "~" H 5600 4300 50  0001 C CNN
	1    5600 4300
	-1   0    0    1   
$EndComp
$Comp
L power:+5VA #PWR017
U 1 1 606A4126
P 5600 4050
F 0 "#PWR017" H 5600 3900 50  0001 C CNN
F 1 "+5VA" H 5615 4223 50  0000 C CNN
F 2 "" H 5600 4050 50  0001 C CNN
F 3 "" H 5600 4050 50  0001 C CNN
	1    5600 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 4050 5600 4150
Wire Wire Line
	5350 4750 5600 4750
Text Label 5900 4750 2    50   ~ 0
RX_TFT
Wire Wire Line
	5600 4450 5600 4750
Wire Wire Line
	4650 4450 4650 4750
Wire Wire Line
	5900 4750 5600 4750
Connection ~ 5600 4750
Text Label 6000 4100 0    50   ~ 0
RX_TFT
Wire Wire Line
	6000 4100 6300 4100
Wire Wire Line
	5900 4300 6300 4300
$Comp
L power:GNDA #PWR019
U 1 1 606DA4BB
P 6200 4450
F 0 "#PWR019" H 6200 4200 50  0001 C CNN
F 1 "GNDA" H 6205 4277 50  0000 C CNN
F 2 "" H 6200 4450 50  0001 C CNN
F 3 "" H 6200 4450 50  0001 C CNN
	1    6200 4450
	1    0    0    -1  
$EndComp
Wire Notes Line
	4200 3800 4200 6400
Wire Notes Line
	4200 6400 650  6400
Wire Notes Line
	650  6400 650  3800
Wire Notes Line
	650  3800 4200 3800
$Comp
L power:GNDA #PWR0102
U 1 1 6079A9CF
P 8850 2500
F 0 "#PWR0102" H 8850 2250 50  0001 C CNN
F 1 "GNDA" H 8855 2327 50  0000 C CNN
F 2 "" H 8850 2500 50  0001 C CNN
F 3 "" H 8850 2500 50  0001 C CNN
	1    8850 2500
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR0103
U 1 1 6079AC1D
P 9250 2500
F 0 "#PWR0103" H 9250 2250 50  0001 C CNN
F 1 "GNDA" H 9255 2327 50  0000 C CNN
F 2 "" H 9250 2500 50  0001 C CNN
F 3 "" H 9250 2500 50  0001 C CNN
	1    9250 2500
	1    0    0    -1  
$EndComp
$Comp
L Transistor_BJT:BC547 Q1
U 1 1 607C9C5F
P 7200 3000
F 0 "Q1" H 7391 3046 50  0000 L CNN
F 1 "BC547" H 7391 2955 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline_Wide" H 7400 2925 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/BC550-D.pdf" H 7200 3000 50  0001 L CNN
	1    7200 3000
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C5
U 1 1 607DEBD5
P 4000 2300
F 0 "C5" H 4118 2346 50  0000 L CNN
F 1 "220uF" H 4118 2255 50  0000 L CNN
F 2 "Capacitor_THT:C_Radial_D6.3mm_H11.0mm_P2.50mm" H 4038 2150 50  0001 C CNN
F 3 "~" H 4000 2300 50  0001 C CNN
	1    4000 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 2100 4150 2050
Wire Wire Line
	3650 2100 4000 2100
Wire Wire Line
	4000 2150 4000 2100
Connection ~ 4000 2100
Wire Wire Line
	4000 2100 4150 2100
$Comp
L power:GND #PWR07
U 1 1 607EAF15
P 4000 2550
F 0 "#PWR07" H 4000 2300 50  0001 C CNN
F 1 "GND" H 4005 2377 50  0000 C CNN
F 2 "" H 4000 2550 50  0001 C CNN
F 3 "" H 4000 2550 50  0001 C CNN
	1    4000 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 2550 4000 2450
$Comp
L Device:Varistor RV1
U 1 1 607F15FB
P 1650 1100
F 0 "RV1" H 1350 1150 50  0000 L CNN
F 1 "10D561" H 1250 1050 50  0000 L CNN
F 2 "Varistor:RV_Disc_D12mm_W3.9mm_P7.5mm" V 1580 1100 50  0001 C CNN
F 3 "~" H 1650 1100 50  0001 C CNN
	1    1650 1100
	1    0    0    -1  
$EndComp
$Comp
L Device:EMI_Filter_CommonMode FL1
U 1 1 60800C80
P 2500 1050
F 0 "FL1" H 2500 1450 50  0000 C CNN
F 1 "EMI_Filter_CommonMode" H 2500 1300 50  0000 C CNN
F 2 "Inductor_THT:L_CommonMode_Toroid_Vertical_L19.3mm_W10.8mm_Px6.35mm_Py15.24mm_Bourns_8100" H 2500 1090 50  0001 C CNN
F 3 "~" H 2500 1090 50  0001 C CNN
	1    2500 1050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 6080AB3D
P 1900 1100
F 0 "C2" H 2015 1146 50  0000 L CNN
F 1 "104/630V" H 1900 1000 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L27.0mm_W9.0mm_P23.00mm" H 1938 950 50  0001 C CNN
F 3 "~" H 1900 1100 50  0001 C CNN
	1    1900 1100
	1    0    0    -1  
$EndComp
$Comp
L Device:Fuse F1
U 1 1 6083CD50
P 1350 850
F 0 "F1" V 1153 850 50  0000 C CNN
F 1 "Fuse" V 1244 850 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P20.32mm_Horizontal" V 1280 850 50  0001 C CNN
F 3 "~" H 1350 850 50  0001 C CNN
	1    1350 850 
	0    1    1    0   
$EndComp
Wire Wire Line
	1050 850  1050 1050
Wire Wire Line
	2300 1150 2300 1350
Wire Wire Line
	1050 1150 1050 1350
Wire Wire Line
	2300 850  2300 950 
Wire Wire Line
	1050 850  1200 850 
Wire Wire Line
	1500 850  1650 850 
Wire Wire Line
	1650 950  1650 850 
Connection ~ 1650 850 
Wire Wire Line
	1650 850  1900 850 
Wire Wire Line
	1900 950  1900 850 
Connection ~ 1900 850 
Wire Wire Line
	1900 850  2300 850 
Wire Wire Line
	2300 1350 1900 1350
Wire Wire Line
	1650 1250 1650 1350
Connection ~ 1650 1350
Wire Wire Line
	1650 1350 1050 1350
Wire Wire Line
	1900 1250 1900 1350
Connection ~ 1900 1350
Wire Wire Line
	1900 1350 1650 1350
Wire Wire Line
	2850 950  2700 950 
Wire Wire Line
	2850 1150 2700 1150
$Comp
L Device:Varistor RV2
U 1 1 608BF74D
P 1650 2250
F 0 "RV2" H 1350 2300 50  0000 L CNN
F 1 "10D561" H 1250 2200 50  0000 L CNN
F 2 "Varistor:RV_Disc_D12mm_W3.9mm_P7.5mm" V 1580 2250 50  0001 C CNN
F 3 "~" H 1650 2250 50  0001 C CNN
	1    1650 2250
	1    0    0    -1  
$EndComp
$Comp
L Device:EMI_Filter_CommonMode FL2
U 1 1 608BF753
P 2500 2200
F 0 "FL2" H 2500 2600 50  0000 C CNN
F 1 "EMI_Filter_CommonMode" H 2500 2450 50  0000 C CNN
F 2 "Inductor_THT:L_CommonMode_Toroid_Vertical_L19.3mm_W10.8mm_Px6.35mm_Py15.24mm_Bourns_8100" H 2500 2240 50  0001 C CNN
F 3 "~" H 2500 2240 50  0001 C CNN
	1    2500 2200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 608BF759
P 1900 2250
F 0 "C3" H 2015 2296 50  0000 L CNN
F 1 "104/630V" H 1900 2150 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L27.0mm_W9.0mm_P23.00mm" H 1938 2100 50  0001 C CNN
F 3 "~" H 1900 2250 50  0001 C CNN
	1    1900 2250
	1    0    0    -1  
$EndComp
$Comp
L Device:Fuse F2
U 1 1 608BF75F
P 1350 2000
F 0 "F2" V 1153 2000 50  0000 C CNN
F 1 "Fuse" V 1244 2000 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P20.32mm_Horizontal" V 1280 2000 50  0001 C CNN
F 3 "~" H 1350 2000 50  0001 C CNN
	1    1350 2000
	0    1    1    0   
$EndComp
Wire Wire Line
	2300 2300 2300 2500
Wire Wire Line
	2300 2000 2300 2100
Wire Wire Line
	1500 2000 1650 2000
Wire Wire Line
	1650 2100 1650 2000
Connection ~ 1650 2000
Wire Wire Line
	1650 2000 1900 2000
Wire Wire Line
	1900 2100 1900 2000
Connection ~ 1900 2000
Wire Wire Line
	1900 2000 2300 2000
Wire Wire Line
	2300 2500 1900 2500
Wire Wire Line
	1650 2400 1650 2500
Connection ~ 1650 2500
Wire Wire Line
	1650 2500 1050 2500
Wire Wire Line
	1900 2400 1900 2500
Connection ~ 1900 2500
Wire Wire Line
	1900 2500 1650 2500
Wire Wire Line
	1050 2250 1050 2500
Wire Wire Line
	1200 2000 1050 2000
Wire Wire Line
	1050 2000 1050 2150
Wire Notes Line
	650  3700 650  1750
Wire Notes Line
	650  1750 4450 1750
Wire Notes Line
	4450 1750 4450 3700
Wire Notes Line
	650  3700 4450 3700
Wire Wire Line
	4250 950  4650 950 
Wire Notes Line
	5050 550  5050 1650
Wire Notes Line
	5050 1650 650  1650
Wire Notes Line
	650  1650 650  550 
Wire Notes Line
	650  550  5050 550 
Wire Notes Line
	9550 550  9550 3600
Wire Notes Line
	9550 3600 5150 3600
Wire Notes Line
	5150 3600 5150 550 
Wire Notes Line
	5150 550  9550 550 
Wire Notes Line
	6750 3800 6750 5350
Wire Notes Line
	6750 5350 4300 5350
Wire Notes Line
	4300 5350 4300 3800
Wire Notes Line
	4300 3800 6750 3800
$EndSCHEMATC
