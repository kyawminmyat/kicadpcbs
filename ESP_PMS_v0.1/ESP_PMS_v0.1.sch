EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 2
Title "A9 Max485 Iso v0.1"
Date "2021-01-11"
Rev "0"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Mechanical:MountingHole H1
U 1 1 600C6DA6
P 6250 6850
F 0 "H1" H 6350 6896 50  0000 L CNN
F 1 "MountingHole" H 6350 6805 50  0000 L CNN
F 2 "MountingHole:MountingHole_3mm_Pad_Via" H 6250 6850 50  0001 C CNN
F 3 "~" H 6250 6850 50  0001 C CNN
	1    6250 6850
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 600722EB
P 6250 7100
F 0 "H2" H 6350 7146 50  0000 L CNN
F 1 "MountingHole" H 6350 7055 50  0000 L CNN
F 2 "MountingHole:MountingHole_3mm_Pad_Via" H 6250 7100 50  0001 C CNN
F 3 "~" H 6250 7100 50  0001 C CNN
	1    6250 7100
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 60062703
P 6250 7350
F 0 "H3" H 6350 7396 50  0000 L CNN
F 1 "MountingHole" H 6350 7305 50  0000 L CNN
F 2 "MountingHole:MountingHole_3mm_Pad_Via" H 6250 7350 50  0001 C CNN
F 3 "~" H 6250 7350 50  0001 C CNN
	1    6250 7350
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 602E7F78
P 6250 7600
F 0 "H4" H 6350 7646 50  0000 L CNN
F 1 "MountingHole" H 6350 7555 50  0000 L CNN
F 2 "MountingHole:MountingHole_3mm_Pad_Via" H 6250 7600 50  0001 C CNN
F 3 "~" H 6250 7600 50  0001 C CNN
	1    6250 7600
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C5
U 1 1 602E7F73
P 2200 1950
F 0 "C5" H 2318 1996 50  0000 L CNN
F 1 "10uF" H 2318 1905 50  0000 L CNN
F 2 "Capacitor_THT:C_Radial_D6.3mm_H11.0mm_P2.50mm" H 2238 1800 50  0001 C CNN
F 3 "~" H 2200 1950 50  0001 C CNN
	1    2200 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 1700 2200 1700
Wire Wire Line
	2450 1700 2450 1650
Wire Wire Line
	2200 1800 2200 1700
Connection ~ 2200 1700
Wire Wire Line
	2200 1700 2450 1700
Wire Wire Line
	1750 2200 1750 2000
Wire Wire Line
	2200 2200 2200 2100
Wire Wire Line
	700  1700 700  1650
Wire Wire Line
	850  1800 850  1700
Wire Wire Line
	850  1700 700  1700
Wire Wire Line
	1250 1800 1250 1700
Wire Wire Line
	850  2200 850  2100
Wire Wire Line
	1250 2200 1250 2100
$Comp
L Device:CP C1
U 1 1 5FFBDF70
P 850 1950
F 0 "C1" H 968 1996 50  0000 L CNN
F 1 "10uF" H 968 1905 50  0000 L CNN
F 2 "Capacitor_THT:C_Radial_D6.3mm_H11.0mm_P2.50mm" H 888 1800 50  0001 C CNN
F 3 "~" H 850 1950 50  0001 C CNN
	1    850  1950
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 5FFBDF7C
P 1250 1950
F 0 "C3" H 1365 1996 50  0000 L CNN
F 1 "104" H 1365 1905 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W3.5mm_P5.00mm" H 1288 1800 50  0001 C CNN
F 3 "~" H 1250 1950 50  0001 C CNN
	1    1250 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	1450 1700 1250 1700
$Comp
L Device:C C9
U 1 1 600C6DB4
P 10750 2250
F 0 "C9" H 10865 2296 50  0000 L CNN
F 1 "104" H 10865 2205 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W3.5mm_P5.00mm" H 10788 2100 50  0001 C CNN
F 3 "~" H 10750 2250 50  0001 C CNN
	1    10750 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	10750 2100 10750 2000
Wire Wire Line
	10750 2400 10750 2500
$Comp
L Connector:Screw_Terminal_01x02 J1
U 1 1 6008BAF9
P 700 1050
F 0 "J1" H 750 750 50  0000 C CNN
F 1 "PowerIN" H 650 850 50  0000 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 700 1050 50  0001 C CNN
F 3 "~" H 700 1050 50  0001 C CNN
	1    700  1050
	-1   0    0    1   
$EndComp
Wire Wire Line
	2750 900  2600 900 
Wire Wire Line
	950  950  900  950 
Wire Wire Line
	2750 1100 2600 1100
Wire Wire Line
	950  1050 900  1050
Wire Wire Line
	3550 1100 3600 1100
Wire Wire Line
	3600 1100 3600 1350
$Comp
L Interface_UART:MAX485E U12
U 1 1 600F0FB5
P 9300 1450
F 0 "U12" H 8900 2000 50  0000 C CNN
F 1 "MAX485E" H 9000 1900 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 9300 750 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX1487E-MAX491E.pdf" H 9300 1500 50  0001 C CNN
	1    9300 1450
	1    0    0    -1  
$EndComp
$Comp
L Isolator:PC817 U9
U 1 1 600F3191
P 7400 3100
F 0 "U9" H 7400 3425 50  0000 C CNN
F 1 "PC817" H 7400 3334 50  0000 C CNN
F 2 "Package_DIP:DIP-4_W7.62mm" H 7200 2900 50  0001 L CIN
F 3 "http://www.soselectronic.cz/a_info/resource/d/pc817.pdf" H 7400 3100 50  0001 L CNN
	1    7400 3100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R20
U 1 1 600C6DAE
P 7000 2550
F 0 "R20" H 7100 2450 50  0000 L CNN
F 1 "120R" H 7050 2550 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 6930 2550 50  0001 C CNN
F 3 "~" H 7000 2550 50  0001 C CNN
	1    7000 2550
	-1   0    0    1   
$EndComp
Wire Wire Line
	7100 3000 7000 3000
Wire Wire Line
	7000 2400 7000 2300
$Comp
L power:GNDA #PWR043
U 1 1 6014F9C4
P 7800 3350
F 0 "#PWR043" H 7800 3100 50  0001 C CNN
F 1 "GNDA" H 7805 3177 50  0000 C CNN
F 2 "" H 7800 3350 50  0001 C CNN
F 3 "" H 7800 3350 50  0001 C CNN
	1    7800 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	7700 3200 7800 3200
Wire Wire Line
	7800 3200 7800 3350
$Comp
L Device:R R24
U 1 1 600C6DB0
P 7950 2550
F 0 "R24" H 8050 2450 50  0000 L CNN
F 1 "1k" H 8000 2550 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 7880 2550 50  0001 C CNN
F 3 "~" H 7950 2550 50  0001 C CNN
	1    7950 2550
	-1   0    0    1   
$EndComp
$Comp
L Device:R R26
U 1 1 600C6DB1
P 8250 3000
F 0 "R26" V 8450 3000 50  0000 L CNN
F 1 "10k" V 8350 2950 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 8180 3000 50  0001 C CNN
F 3 "~" H 8250 3000 50  0001 C CNN
	1    8250 3000
	0    -1   -1   0   
$EndComp
Connection ~ 7950 3000
Wire Wire Line
	7950 3000 8100 3000
$Comp
L power:+5VA #PWR045
U 1 1 600C6DB2
P 7950 2300
F 0 "#PWR045" H 7950 2150 50  0001 C CNN
F 1 "+5VA" H 7965 2473 50  0000 C CNN
F 2 "" H 7950 2300 50  0001 C CNN
F 3 "" H 7950 2300 50  0001 C CNN
	1    7950 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	7950 2300 7950 2400
Wire Wire Line
	8500 3000 8400 3000
$Comp
L power:GNDA #PWR048
U 1 1 600C6DB3
P 8800 3350
F 0 "#PWR048" H 8800 3100 50  0001 C CNN
F 1 "GNDA" H 8805 3177 50  0000 C CNN
F 2 "" H 8800 3350 50  0001 C CNN
F 3 "" H 8800 3350 50  0001 C CNN
	1    8800 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	8800 3350 8800 3200
Wire Wire Line
	7700 3000 7950 3000
Text Label 8250 3250 2    50   ~ 0
DI
Wire Wire Line
	8250 3250 7950 3250
Wire Wire Line
	7950 3250 7950 3000
$Comp
L Device:R R27
U 1 1 6022E61E
P 8800 2550
F 0 "R27" H 8900 2450 50  0000 L CNN
F 1 "1k" H 8850 2550 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 8730 2550 50  0001 C CNN
F 3 "~" H 8800 2550 50  0001 C CNN
	1    8800 2550
	-1   0    0    1   
$EndComp
Wire Wire Line
	8800 2800 8800 2750
$Comp
L power:+5VA #PWR047
U 1 1 60244D75
P 8800 2300
F 0 "#PWR047" H 8800 2150 50  0001 C CNN
F 1 "+5VA" H 8815 2473 50  0000 C CNN
F 2 "" H 8800 2300 50  0001 C CNN
F 3 "" H 8800 2300 50  0001 C CNN
	1    8800 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	8800 2400 8800 2300
Wire Wire Line
	7950 2700 7950 3000
Wire Wire Line
	7000 2700 7000 3000
Text Label 9100 2750 2    50   ~ 0
DE,RE
Wire Wire Line
	9100 2750 8800 2750
Connection ~ 8800 2750
Wire Wire Line
	8800 2750 8800 2700
Text Label 8550 1450 0    50   ~ 0
DE,RE
Wire Wire Line
	8900 1450 8800 1450
Wire Wire Line
	8900 1550 8800 1550
Wire Wire Line
	8800 1550 8800 1450
Connection ~ 8800 1450
Wire Wire Line
	8800 1450 8550 1450
$Comp
L power:GNDA #PWR050
U 1 1 600C6DB7
P 9300 2150
F 0 "#PWR050" H 9300 1900 50  0001 C CNN
F 1 "GNDA" H 9305 1977 50  0000 C CNN
F 2 "" H 9300 2150 50  0001 C CNN
F 3 "" H 9300 2150 50  0001 C CNN
	1    9300 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	9300 2150 9300 2050
$Comp
L power:+5VA #PWR049
U 1 1 600C6DB8
P 9300 800
F 0 "#PWR049" H 9300 650 50  0001 C CNN
F 1 "+5VA" H 9315 973 50  0000 C CNN
F 2 "" H 9300 800 50  0001 C CNN
F 3 "" H 9300 800 50  0001 C CNN
	1    9300 800 
	1    0    0    -1  
$EndComp
Wire Wire Line
	9300 950  9300 800 
Text Label 8550 1650 0    50   ~ 0
DI
Wire Wire Line
	8550 1650 8900 1650
Text Label 8550 1350 0    50   ~ 0
RO
Wire Wire Line
	8550 1350 8900 1350
$Comp
L Isolator:PC817 U10
U 1 1 600C6DB9
P 7450 1550
F 0 "U10" H 7450 1875 50  0000 C CNN
F 1 "PC817" H 7450 1784 50  0000 C CNN
F 2 "Package_DIP:DIP-4_W7.62mm" H 7250 1350 50  0001 L CIN
F 3 "http://www.soselectronic.cz/a_info/resource/d/pc817.pdf" H 7450 1550 50  0001 L CNN
	1    7450 1550
	-1   0    0    -1  
$EndComp
Text Label 8050 1650 2    50   ~ 0
RO
Wire Wire Line
	8050 1650 7750 1650
$Comp
L Device:R R23
U 1 1 600C6DBA
P 7900 1200
F 0 "R23" H 8000 1100 50  0000 L CNN
F 1 "330R" H 7950 1200 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 7830 1200 50  0001 C CNN
F 3 "~" H 7900 1200 50  0001 C CNN
	1    7900 1200
	-1   0    0    1   
$EndComp
Wire Wire Line
	7750 1450 7900 1450
Wire Wire Line
	7900 1450 7900 1350
$Comp
L power:+5VA #PWR044
U 1 1 600C6DBB
P 7900 950
F 0 "#PWR044" H 7900 800 50  0001 C CNN
F 1 "+5VA" H 7915 1123 50  0000 C CNN
F 2 "" H 7900 950 50  0001 C CNN
F 3 "" H 7900 950 50  0001 C CNN
	1    7900 950 
	1    0    0    -1  
$EndComp
Wire Wire Line
	7900 1050 7900 950 
$Comp
L power:GND #PWR039
U 1 1 600C6DBC
P 7050 1750
F 0 "#PWR039" H 7050 1500 50  0001 C CNN
F 1 "GND" H 7055 1577 50  0000 C CNN
F 2 "" H 7050 1750 50  0001 C CNN
F 3 "" H 7050 1750 50  0001 C CNN
	1    7050 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	7050 1750 7050 1650
Wire Wire Line
	7050 1650 7150 1650
Wire Wire Line
	7150 1450 7000 1450
$Comp
L Device:R R19
U 1 1 600C6DBD
P 7000 1200
F 0 "R19" H 7100 1100 50  0000 L CNN
F 1 "470R" H 7050 1200 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 6930 1200 50  0001 C CNN
F 3 "~" H 7000 1200 50  0001 C CNN
	1    7000 1200
	-1   0    0    1   
$EndComp
Wire Wire Line
	7000 1350 7000 1450
Wire Wire Line
	7000 1050 7000 950 
$Comp
L Connector:Screw_Terminal_01x02 J9
U 1 1 600C6DBE
P 10800 1450
F 0 "J9" H 10750 1700 50  0000 C CNN
F 1 "Mobus_OUT" H 10750 1600 50  0000 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 10800 1450 50  0001 C CNN
F 3 "~" H 10800 1450 50  0001 C CNN
	1    10800 1450
	1    0    0    -1  
$EndComp
$Comp
L power:+5VA #PWR051
U 1 1 604A6C47
P 9900 800
F 0 "#PWR051" H 9900 650 50  0001 C CNN
F 1 "+5VA" H 9915 973 50  0000 C CNN
F 2 "" H 9900 800 50  0001 C CNN
F 3 "" H 9900 800 50  0001 C CNN
	1    9900 800 
	1    0    0    -1  
$EndComp
$Comp
L Device:R R28
U 1 1 600C6DC0
P 9900 1050
F 0 "R28" H 10000 950 50  0000 L CNN
F 1 "20k" H 9950 1050 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 9830 1050 50  0001 C CNN
F 3 "~" H 9900 1050 50  0001 C CNN
	1    9900 1050
	-1   0    0    1   
$EndComp
Wire Wire Line
	9900 900  9900 800 
$Comp
L Device:R R29
U 1 1 600C6DC2
P 10000 1900
F 0 "R29" H 10100 1800 50  0000 L CNN
F 1 "20k" H 10050 1900 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 9930 1900 50  0001 C CNN
F 3 "~" H 10000 1900 50  0001 C CNN
	1    10000 1900
	-1   0    0    1   
$EndComp
$Comp
L Device:R R30
U 1 1 604A58F5
P 10400 1500
F 0 "R30" H 10500 1400 50  0000 L CNN
F 1 "120R" H 10450 1500 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 10330 1500 50  0001 C CNN
F 3 "~" H 10400 1500 50  0001 C CNN
	1    10400 1500
	-1   0    0    1   
$EndComp
Wire Wire Line
	9900 1200 9900 1650
Wire Wire Line
	9900 1650 9700 1650
Wire Wire Line
	9700 1350 10000 1350
Wire Wire Line
	10000 1350 10000 1750
Wire Wire Line
	10400 1350 10000 1350
Connection ~ 10000 1350
Wire Wire Line
	10400 1650 9900 1650
Connection ~ 9900 1650
Wire Wire Line
	10400 1350 10600 1350
Wire Wire Line
	10600 1350 10600 1450
Connection ~ 10400 1350
Wire Wire Line
	10600 1550 10600 1650
Wire Wire Line
	10600 1650 10400 1650
Connection ~ 10400 1650
$Comp
L power:GNDA #PWR052
U 1 1 600C6DC3
P 10000 2150
F 0 "#PWR052" H 10000 1900 50  0001 C CNN
F 1 "GNDA" H 10005 1977 50  0000 C CNN
F 2 "" H 10000 2150 50  0001 C CNN
F 3 "" H 10000 2150 50  0001 C CNN
	1    10000 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	10000 2150 10000 2050
$Comp
L power:+5VA #PWR055
U 1 1 600C6DC4
P 10750 2000
F 0 "#PWR055" H 10750 1850 50  0001 C CNN
F 1 "+5VA" H 10765 2173 50  0000 C CNN
F 2 "" H 10750 2000 50  0001 C CNN
F 3 "" H 10750 2000 50  0001 C CNN
	1    10750 2000
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR056
U 1 1 6079AC1D
P 10750 2500
F 0 "#PWR056" H 10750 2250 50  0001 C CNN
F 1 "GNDA" H 10755 2327 50  0000 C CNN
F 2 "" H 10750 2500 50  0001 C CNN
F 3 "" H 10750 2500 50  0001 C CNN
	1    10750 2500
	1    0    0    -1  
$EndComp
$Comp
L Transistor_BJT:BC547 Q4
U 1 1 607C9C5F
P 8700 3000
F 0 "Q4" H 8891 3046 50  0000 L CNN
F 1 "BC547" H 8891 2955 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline_Wide" H 8900 2925 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/BC550-D.pdf" H 8700 3000 50  0001 L CNN
	1    8700 3000
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C7
U 1 1 607DEBD5
P 3900 1100
F 0 "C7" H 4018 1146 50  0000 L CNN
F 1 "220uF" H 4018 1055 50  0000 L CNN
F 2 "Capacitor_THT:C_Radial_D6.3mm_H11.0mm_P2.50mm" H 3938 950 50  0001 C CNN
F 3 "~" H 3900 1100 50  0001 C CNN
	1    3900 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4050 900  4050 850 
Wire Wire Line
	3550 900  3900 900 
Wire Wire Line
	3900 950  3900 900 
Connection ~ 3900 900 
Wire Wire Line
	3900 900  4050 900 
Wire Wire Line
	3900 1350 3900 1250
$Comp
L Device:Varistor RV1
U 1 1 608BF74D
P 1550 1050
F 0 "RV1" H 1250 1100 50  0000 L CNN
F 1 "10D561" H 1150 1000 50  0000 L CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" V 1480 1050 50  0001 C CNN
F 3 "~" H 1550 1050 50  0001 C CNN
	1    1550 1050
	1    0    0    -1  
$EndComp
$Comp
L Device:EMI_Filter_CommonMode FL1
U 1 1 600C6DC9
P 2400 1000
F 0 "FL1" H 2400 1400 50  0000 C CNN
F 1 "EMI_Filter_CommonMode" H 2400 1250 50  0000 C CNN
F 2 "Inductor_THT:L_CommonMode_Toroid_Vertical_L19.3mm_W10.8mm_Px6.35mm_Py15.24mm_Bourns_8100" H 2400 1040 50  0001 C CNN
F 3 "~" H 2400 1040 50  0001 C CNN
	1    2400 1000
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 608BF759
P 1800 1050
F 0 "C4" H 1915 1096 50  0000 L CNN
F 1 "104/630V" H 1800 950 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L19.0mm_W6.0mm_P15.00mm_MKS4" H 1838 900 50  0001 C CNN
F 3 "~" H 1800 1050 50  0001 C CNN
	1    1800 1050
	1    0    0    -1  
$EndComp
$Comp
L Device:Fuse F1
U 1 1 608BF75F
P 1250 800
F 0 "F1" V 1053 800 50  0000 C CNN
F 1 "Fuse" V 1144 800 50  0000 C CNN
F 2 "Fuse:Fuseholder_Cylinder-5x20mm_Stelvio-Kontek_PTF78_Horizontal_Open" V 1180 800 50  0001 C CNN
F 3 "~" H 1250 800 50  0001 C CNN
	1    1250 800 
	0    1    1    0   
$EndComp
Wire Wire Line
	2200 1100 2200 1300
Wire Wire Line
	2200 800  2200 900 
Wire Wire Line
	1400 800  1550 800 
Wire Wire Line
	1550 900  1550 800 
Connection ~ 1550 800 
Wire Wire Line
	1550 800  1800 800 
Wire Wire Line
	1800 900  1800 800 
Connection ~ 1800 800 
Wire Wire Line
	1800 800  2200 800 
Wire Wire Line
	2200 1300 1800 1300
Wire Wire Line
	1550 1200 1550 1300
Connection ~ 1550 1300
Wire Wire Line
	1550 1300 950  1300
Wire Wire Line
	1800 1200 1800 1300
Connection ~ 1800 1300
Wire Wire Line
	1800 1300 1550 1300
Wire Wire Line
	950  1050 950  1300
Wire Wire Line
	1100 800  950  800 
Wire Wire Line
	950  800  950  950 
Wire Notes Line
	550  2500 550  550 
Wire Notes Line
	550  550  4350 550 
Wire Notes Line
	4350 550  4350 2500
Wire Notes Line
	550  2500 4350 2500
Wire Notes Line
	11050 550  11050 3600
Wire Notes Line
	11050 3600 6650 3600
Wire Notes Line
	6650 3600 6650 550 
Wire Notes Line
	6650 550  11050 550 
$Comp
L Regulator_Linear:AMS1117-5.0 U4
U 1 1 60BCEE74
P 1750 1700
F 0 "U4" H 1750 1942 50  0000 C CNN
F 1 "AMS1117-5.0" H 1750 1851 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-223-3_TabPin2" H 1750 1900 50  0001 C CNN
F 3 "http://www.advanced-monolithic.com/pdf/ds1117.pdf" H 1850 1450 50  0001 C CNN
	1    1750 1700
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR03
U 1 1 60BE0C7F
P 850 2200
F 0 "#PWR03" H 850 1950 50  0001 C CNN
F 1 "GNDA" H 855 2027 50  0000 C CNN
F 2 "" H 850 2200 50  0001 C CNN
F 3 "" H 850 2200 50  0001 C CNN
	1    850  2200
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR05
U 1 1 60BF453C
P 1250 2200
F 0 "#PWR05" H 1250 1950 50  0001 C CNN
F 1 "GNDA" H 1255 2027 50  0000 C CNN
F 2 "" H 1250 2200 50  0001 C CNN
F 3 "" H 1250 2200 50  0001 C CNN
	1    1250 2200
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR010
U 1 1 60BF471E
P 1750 2200
F 0 "#PWR010" H 1750 1950 50  0001 C CNN
F 1 "GNDA" H 1755 2027 50  0000 C CNN
F 2 "" H 1750 2200 50  0001 C CNN
F 3 "" H 1750 2200 50  0001 C CNN
	1    1750 2200
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR011
U 1 1 60BF4998
P 2200 2200
F 0 "#PWR011" H 2200 1950 50  0001 C CNN
F 1 "GNDA" H 2205 2027 50  0000 C CNN
F 2 "" H 2200 2200 50  0001 C CNN
F 3 "" H 2200 2200 50  0001 C CNN
	1    2200 2200
	1    0    0    -1  
$EndComp
$Comp
L power:+5VA #PWR013
U 1 1 60BF4BE1
P 2450 1650
F 0 "#PWR013" H 2450 1500 50  0001 C CNN
F 1 "+5VA" H 2465 1823 50  0000 C CNN
F 2 "" H 2450 1650 50  0001 C CNN
F 3 "" H 2450 1650 50  0001 C CNN
	1    2450 1650
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR016
U 1 1 60BF56DD
P 3600 1350
F 0 "#PWR016" H 3600 1100 50  0001 C CNN
F 1 "GNDA" H 3605 1177 50  0000 C CNN
F 2 "" H 3600 1350 50  0001 C CNN
F 3 "" H 3600 1350 50  0001 C CNN
	1    3600 1350
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR017
U 1 1 60BF5A83
P 3900 1350
F 0 "#PWR017" H 3900 1100 50  0001 C CNN
F 1 "GNDA" H 3905 1177 50  0000 C CNN
F 2 "" H 3900 1350 50  0001 C CNN
F 3 "" H 3900 1350 50  0001 C CNN
	1    3900 1350
	1    0    0    -1  
$EndComp
$Comp
L power:+12VA #PWR018
U 1 1 60BF61D6
P 4050 850
F 0 "#PWR018" H 4050 700 50  0001 C CNN
F 1 "+12VA" H 4065 1023 50  0000 C CNN
F 2 "" H 4050 850 50  0001 C CNN
F 3 "" H 4050 850 50  0001 C CNN
	1    4050 850 
	1    0    0    -1  
$EndComp
$Comp
L power:+12VA #PWR01
U 1 1 60BF7392
P 700 1650
F 0 "#PWR01" H 700 1500 50  0001 C CNN
F 1 "+12VA" H 715 1823 50  0000 C CNN
F 2 "" H 700 1650 50  0001 C CNN
F 3 "" H 700 1650 50  0001 C CNN
	1    700  1650
	1    0    0    -1  
$EndComp
$Comp
L My_Lib_KiCAD:EVISUN U5
U 1 1 60C115E4
P 1900 3100
F 0 "U5" H 1900 3467 50  0000 C CNN
F 1 "EVISUN" H 1900 3376 50  0000 C CNN
F 2 "My_Lib_KiCAD:EVISUN_2W" H 1750 3450 50  0001 C CNN
F 3 "" H 1700 3400 50  0001 C CNN
	1    1900 3100
	1    0    0    -1  
$EndComp
$Comp
L power:+12VA #PWR02
U 1 1 60C13147
P 700 2900
F 0 "#PWR02" H 700 2750 50  0001 C CNN
F 1 "+12VA" H 715 3073 50  0000 C CNN
F 2 "" H 700 2900 50  0001 C CNN
F 3 "" H 700 2900 50  0001 C CNN
	1    700  2900
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C2
U 1 1 60C13596
P 1000 3200
F 0 "C2" H 1118 3246 50  0000 L CNN
F 1 "2.2uF" H 1118 3155 50  0000 L CNN
F 2 "Capacitor_THT:C_Radial_D6.3mm_H11.0mm_P2.50mm" H 1038 3050 50  0001 C CNN
F 3 "~" H 1000 3200 50  0001 C CNN
	1    1000 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 3000 1000 3000
Wire Wire Line
	700  3000 700  2900
Wire Wire Line
	1000 3050 1000 3000
Connection ~ 1000 3000
Wire Wire Line
	1000 3000 700  3000
$Comp
L Device:CP C6
U 1 1 60C20683
P 2700 3200
F 0 "C6" H 2818 3246 50  0000 L CNN
F 1 "220uF" H 2818 3155 50  0000 L CNN
F 2 "Capacitor_THT:C_Radial_D6.3mm_H11.0mm_P2.50mm" H 2738 3050 50  0001 C CNN
F 3 "~" H 2700 3200 50  0001 C CNN
	1    2700 3200
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR015
U 1 1 60C20EAD
P 3100 2900
F 0 "#PWR015" H 3100 2750 50  0001 C CNN
F 1 "+5V" H 3115 3073 50  0000 C CNN
F 2 "" H 3100 2900 50  0001 C CNN
F 3 "" H 3100 2900 50  0001 C CNN
	1    3100 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 3000 2700 3000
Wire Wire Line
	3100 3000 3100 2900
Wire Wire Line
	2700 3050 2700 3000
Connection ~ 2700 3000
Wire Wire Line
	2700 3000 3100 3000
$Comp
L power:GND #PWR014
U 1 1 60C2EB1F
P 2700 3450
F 0 "#PWR014" H 2700 3200 50  0001 C CNN
F 1 "GND" H 2705 3277 50  0000 C CNN
F 2 "" H 2700 3450 50  0001 C CNN
F 3 "" H 2700 3450 50  0001 C CNN
	1    2700 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	1000 3450 1000 3350
Wire Wire Line
	2700 3450 2700 3350
$Comp
L power:GND #PWR012
U 1 1 60C3C272
P 2400 3450
F 0 "#PWR012" H 2400 3200 50  0001 C CNN
F 1 "GND" H 2405 3277 50  0000 C CNN
F 2 "" H 2400 3450 50  0001 C CNN
F 3 "" H 2400 3450 50  0001 C CNN
	1    2400 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 3450 2400 3200
Wire Wire Line
	2400 3200 2300 3200
Wire Wire Line
	1400 3450 1400 3200
Wire Wire Line
	1400 3200 1500 3200
$Comp
L power:GNDA #PWR04
U 1 1 60C49DDE
P 1000 3450
F 0 "#PWR04" H 1000 3200 50  0001 C CNN
F 1 "GNDA" H 1005 3277 50  0000 C CNN
F 2 "" H 1000 3450 50  0001 C CNN
F 3 "" H 1000 3450 50  0001 C CNN
	1    1000 3450
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR09
U 1 1 60C4A0EC
P 1400 3450
F 0 "#PWR09" H 1400 3200 50  0001 C CNN
F 1 "GNDA" H 1405 3277 50  0000 C CNN
F 2 "" H 1400 3450 50  0001 C CNN
F 3 "" H 1400 3450 50  0001 C CNN
	1    1400 3450
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J7
U 1 1 60C8BB4A
P 5850 2000
F 0 "J7" H 5900 1700 50  0000 C CNN
F 1 "BATT" H 5800 1800 50  0000 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 5850 2000 50  0001 C CNN
F 3 "~" H 5850 2000 50  0001 C CNN
	1    5850 2000
	-1   0    0    1   
$EndComp
$Comp
L Relay_SolidState:MOC3041M U2
U 1 1 60D14CB8
P 1350 4300
F 0 "U2" H 1350 4625 50  0000 C CNN
F 1 "MOC3041M" H 1350 4534 50  0000 C CNN
F 2 "Package_DIP:DIP-6_W7.62mm" H 1150 4100 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/MOC3043M-D.pdf" H 1350 4300 50  0001 L CNN
	1    1350 4300
	1    0    0    -1  
$EndComp
$Comp
L Triac_Thyristor:BTA16-600B Q2
U 1 1 60D14CBE
P 2350 4450
F 0 "Q2" H 2479 4496 50  0000 L CNN
F 1 "BTA16-600B" H 2479 4405 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 2550 4375 50  0001 L CIN
F 3 "https://www.st.com/resource/en/datasheet/bta16.pdf" H 2350 4450 50  0001 L CNN
	1    2350 4450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 60D14CC4
P 1950 4800
F 0 "R2" H 2050 4700 50  0000 L CNN
F 1 "330R" H 2000 4800 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 1880 4800 50  0001 C CNN
F 3 "~" H 1950 4800 50  0001 C CNN
	1    1950 4800
	-1   0    0    1   
$EndComp
$Comp
L Device:R R5
U 1 1 60D14CCA
P 2050 4200
F 0 "R5" V 2250 4200 50  0000 L CNN
F 1 "470R" V 2150 4150 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 1980 4200 50  0001 C CNN
F 3 "~" H 2050 4200 50  0001 C CNN
	1    2050 4200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	650  4200 1050 4200
$Comp
L power:GND #PWR07
U 1 1 60D14CD2
P 950 4500
F 0 "#PWR07" H 950 4250 50  0001 C CNN
F 1 "GND" H 955 4327 50  0000 C CNN
F 2 "" H 950 4500 50  0001 C CNN
F 3 "" H 950 4500 50  0001 C CNN
	1    950  4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	950  4500 950  4400
Wire Wire Line
	950  4400 1050 4400
Wire Wire Line
	2200 4550 1950 4550
Wire Wire Line
	1750 4550 1750 4400
Wire Wire Line
	1750 4400 1650 4400
Wire Wire Line
	1650 4200 1900 4200
Wire Wire Line
	1950 4650 1950 4550
Connection ~ 1950 4550
Wire Wire Line
	1950 4550 1750 4550
$Comp
L Connector:Screw_Terminal_01x02 J3
U 1 1 60D14CE1
P 3300 4350
F 0 "J3" H 3300 4550 50  0000 C CNN
F 1 "L2_OUT" H 3300 4450 50  0000 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 3300 4350 50  0001 C CNN
F 3 "~" H 3300 4350 50  0001 C CNN
	1    3300 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 4350 3000 4350
Wire Wire Line
	3000 4350 3000 4200
Wire Wire Line
	2200 4200 2350 4200
Wire Wire Line
	3100 4450 2950 4450
Wire Wire Line
	2950 4450 2950 5050
Wire Wire Line
	2950 5050 2350 5050
Wire Wire Line
	1950 5050 1950 4950
Wire Wire Line
	2350 4300 2350 4200
Connection ~ 2350 4200
Wire Wire Line
	2350 4200 3000 4200
Wire Wire Line
	2350 4600 2350 5050
Connection ~ 2350 5050
Wire Wire Line
	2350 5050 1950 5050
$Comp
L Relay_SolidState:MOC3041M U3
U 1 1 60D24D76
P 1350 5500
F 0 "U3" H 1350 5825 50  0000 C CNN
F 1 "MOC3041M" H 1350 5734 50  0000 C CNN
F 2 "Package_DIP:DIP-6_W7.62mm" H 1150 5300 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/MOC3043M-D.pdf" H 1350 5500 50  0001 L CNN
	1    1350 5500
	1    0    0    -1  
$EndComp
$Comp
L Triac_Thyristor:BTA16-600B Q3
U 1 1 60D24D7C
P 2350 5650
F 0 "Q3" H 2479 5696 50  0000 L CNN
F 1 "BTA16-600B" H 2479 5605 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 2550 5575 50  0001 L CIN
F 3 "https://www.st.com/resource/en/datasheet/bta16.pdf" H 2350 5650 50  0001 L CNN
	1    2350 5650
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 60D24D82
P 1950 6000
F 0 "R3" H 2050 5900 50  0000 L CNN
F 1 "330R" H 2000 6000 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 1880 6000 50  0001 C CNN
F 3 "~" H 1950 6000 50  0001 C CNN
	1    1950 6000
	-1   0    0    1   
$EndComp
$Comp
L Device:R R6
U 1 1 60D24D88
P 2050 5400
F 0 "R6" V 2250 5400 50  0000 L CNN
F 1 "470R" V 2150 5350 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 1980 5400 50  0001 C CNN
F 3 "~" H 2050 5400 50  0001 C CNN
	1    2050 5400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	650  5400 1050 5400
$Comp
L power:GND #PWR08
U 1 1 60D24D90
P 950 5700
F 0 "#PWR08" H 950 5450 50  0001 C CNN
F 1 "GND" H 955 5527 50  0000 C CNN
F 2 "" H 950 5700 50  0001 C CNN
F 3 "" H 950 5700 50  0001 C CNN
	1    950  5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	950  5700 950  5600
Wire Wire Line
	950  5600 1050 5600
Wire Wire Line
	2200 5750 1950 5750
Wire Wire Line
	1750 5750 1750 5600
Wire Wire Line
	1750 5600 1650 5600
Wire Wire Line
	1650 5400 1900 5400
Wire Wire Line
	1950 5850 1950 5750
Connection ~ 1950 5750
Wire Wire Line
	1950 5750 1750 5750
$Comp
L Connector:Screw_Terminal_01x02 J4
U 1 1 60D24D9F
P 3300 5550
F 0 "J4" H 3300 5750 50  0000 C CNN
F 1 "L3_OUT" H 3300 5650 50  0000 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 3300 5550 50  0001 C CNN
F 3 "~" H 3300 5550 50  0001 C CNN
	1    3300 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 5550 3000 5550
Wire Wire Line
	3000 5550 3000 5400
Wire Wire Line
	2200 5400 2350 5400
Wire Wire Line
	3100 5650 2950 5650
Wire Wire Line
	2950 5650 2950 6250
Wire Wire Line
	2950 6250 2350 6250
Wire Wire Line
	1950 6250 1950 6150
Wire Wire Line
	2350 5500 2350 5400
Connection ~ 2350 5400
Wire Wire Line
	2350 5400 3000 5400
Wire Wire Line
	2350 5800 2350 6250
Connection ~ 2350 6250
Wire Wire Line
	2350 6250 1950 6250
$Comp
L power:+12VA #PWR027
U 1 1 602B2A58
P 5800 950
F 0 "#PWR027" H 5800 800 50  0001 C CNN
F 1 "+12VA" H 5815 1123 50  0000 C CNN
F 2 "" H 5800 950 50  0001 C CNN
F 3 "" H 5800 950 50  0001 C CNN
	1    5800 950 
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 1050 5800 1050
Wire Wire Line
	5800 1050 5800 950 
Text Label 5550 1150 0    50   ~ 0
DI2
Wire Wire Line
	5550 1150 5950 1150
$Comp
L Isolator:PC817 U6
U 1 1 6040D403
P 5500 3150
F 0 "U6" H 5500 3475 50  0000 C CNN
F 1 "PC817" H 5500 3384 50  0000 C CNN
F 2 "Package_DIP:DIP-4_W7.62mm" H 5300 2950 50  0001 L CIN
F 3 "http://www.soselectronic.cz/a_info/resource/d/pc817.pdf" H 5500 3150 50  0001 L CNN
	1    5500 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:R R8
U 1 1 6040D409
P 4900 3050
F 0 "R8" V 5100 3050 50  0000 L CNN
F 1 "10k" V 5000 3000 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 4830 3050 50  0001 C CNN
F 3 "~" H 4900 3050 50  0001 C CNN
	1    4900 3050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5200 3050 5050 3050
Text Label 4500 3050 0    50   ~ 0
DI2
Wire Wire Line
	4500 3050 4750 3050
$Comp
L power:GNDA #PWR021
U 1 1 6040D412
P 5100 3350
F 0 "#PWR021" H 5100 3100 50  0001 C CNN
F 1 "GNDA" H 5105 3177 50  0000 C CNN
F 2 "" H 5100 3350 50  0001 C CNN
F 3 "" H 5100 3350 50  0001 C CNN
	1    5100 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 3250 5100 3250
Wire Wire Line
	5100 3250 5100 3350
$Comp
L power:+5V #PWR026
U 1 1 6040D41A
P 5850 2900
F 0 "#PWR026" H 5850 2750 50  0001 C CNN
F 1 "+5V" H 5865 3073 50  0000 C CNN
F 2 "" H 5850 2900 50  0001 C CNN
F 3 "" H 5850 2900 50  0001 C CNN
	1    5850 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 3050 5850 3050
Wire Wire Line
	5850 3050 5850 2900
$Comp
L Device:R R11
U 1 1 6040D422
P 6050 3550
F 0 "R11" H 6150 3450 50  0000 L CNN
F 1 "3.3k" H 6100 3550 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 5980 3550 50  0001 C CNN
F 3 "~" H 6050 3550 50  0001 C CNN
	1    6050 3550
	-1   0    0    1   
$EndComp
Wire Wire Line
	5800 3250 6050 3250
Wire Wire Line
	6050 3250 6050 3400
$Comp
L Device:R R12
U 1 1 6040D42A
P 6050 4050
F 0 "R12" H 6150 3950 50  0000 L CNN
F 1 "4.7k" H 6100 4050 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 5980 4050 50  0001 C CNN
F 3 "~" H 6050 4050 50  0001 C CNN
	1    6050 4050
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR029
U 1 1 6040D430
P 6050 4300
F 0 "#PWR029" H 6050 4050 50  0001 C CNN
F 1 "GND" H 6055 4127 50  0000 C CNN
F 2 "" H 6050 4300 50  0001 C CNN
F 3 "" H 6050 4300 50  0001 C CNN
	1    6050 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 4300 6050 4200
Wire Wire Line
	6050 3900 6050 3800
Text Label 6400 3800 2    50   ~ 0
IO2
Wire Wire Line
	6400 3800 6050 3800
Connection ~ 6050 3800
Wire Wire Line
	6050 3800 6050 3700
$Comp
L Switch:SW_Push SW1
U 1 1 60586E85
P 4900 1650
F 0 "SW1" V 4854 1798 50  0000 L CNN
F 1 "SW_Push" V 4945 1798 50  0000 L CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 4900 1850 50  0001 C CNN
F 3 "~" H 4900 1850 50  0001 C CNN
	1    4900 1650
	0    1    1    0   
$EndComp
$Comp
L Device:R R7
U 1 1 60589426
P 4900 1150
F 0 "R7" H 5000 1050 50  0000 L CNN
F 1 "1k" H 4950 1150 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 4830 1150 50  0001 C CNN
F 3 "~" H 4900 1150 50  0001 C CNN
	1    4900 1150
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR019
U 1 1 6059A7F8
P 4900 900
F 0 "#PWR019" H 4900 750 50  0001 C CNN
F 1 "+5V" H 4915 1073 50  0000 C CNN
F 2 "" H 4900 900 50  0001 C CNN
F 3 "" H 4900 900 50  0001 C CNN
	1    4900 900 
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 1000 4900 900 
$Comp
L power:GND #PWR020
U 1 1 605AB73D
P 4900 1950
F 0 "#PWR020" H 4900 1700 50  0001 C CNN
F 1 "GND" H 4905 1777 50  0000 C CNN
F 2 "" H 4900 1950 50  0001 C CNN
F 3 "" H 4900 1950 50  0001 C CNN
	1    4900 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 1950 4900 1850
Wire Wire Line
	4900 1450 4900 1400
Text Label 4500 1400 0    50   ~ 0
BUTTON
Wire Wire Line
	4500 1400 4900 1400
Connection ~ 4900 1400
Wire Wire Line
	4900 1400 4900 1300
Wire Wire Line
	1250 1700 850  1700
Connection ~ 1250 1700
Connection ~ 850  1700
$Sheet
S 9300 4850 1600 1150
U 6018463A
F0 "A9_ESP_Max485_Iso_v0.1_sheet2" 50
F1 "A9_ESP_Max485_Iso_v0.1_sheet2.sch" 50
F2 "RX1" I L 9300 5750 50 
F3 "TX1" I L 9300 5650 50 
F4 "STX_TFT" I L 9300 5550 50 
F5 "TRIAC_2" I L 9300 5350 50 
F6 "TRIAC_3" I L 9300 5250 50 
F7 "IO2" I L 9300 5150 50 
$EndSheet
Connection ~ 7000 1450
Text Label 6750 3200 0    50   ~ 0
TX1
Wire Wire Line
	6750 3200 7100 3200
Text Label 8950 5650 0    50   ~ 0
TX1
Text Label 6700 1450 0    50   ~ 0
RX1
Wire Wire Line
	6700 1450 7000 1450
Text Label 8950 5750 0    50   ~ 0
RX1
Wire Wire Line
	8950 5650 9300 5650
Wire Wire Line
	8950 5750 9300 5750
Text Label 650  5400 0    50   ~ 0
TRIAC_3
Text Label 650  4200 0    50   ~ 0
TRIAC_2
Text Label 8950 5350 0    50   ~ 0
TRIAC_2
Text Label 8950 5250 0    50   ~ 0
TRIAC_3
Wire Wire Line
	8950 5350 9300 5350
Wire Wire Line
	8950 5250 9300 5250
$Comp
L Converter_ACDC:HLK-PM12 PS1
U 1 1 60617531
P 3150 1000
F 0 "PS1" H 3150 1325 50  0000 C CNN
F 1 "HLK-PM12" H 3150 1234 50  0000 C CNN
F 2 "Converter_ACDC:Converter_ACDC_HiLink_HLK-PMxx" H 3150 700 50  0001 C CNN
F 3 "http://www.hlktech.net/product_detail.php?ProId=56" H 3550 650 50  0001 C CNN
	1    3150 1000
	1    0    0    -1  
$EndComp
Text Label 8950 5550 0    50   ~ 0
STX_TFT
Wire Wire Line
	9300 5550 8950 5550
$Comp
L Connector:Screw_Terminal_01x02 J2
U 1 1 604583F0
P 6150 1050
F 0 "J2" H 6200 1300 50  0000 C CNN
F 1 "DI_INPUT" H 6200 1200 50  0000 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 6150 1050 50  0001 C CNN
F 3 "~" H 6150 1050 50  0001 C CNN
	1    6150 1050
	1    0    0    -1  
$EndComp
Text Label 8950 5150 0    50   ~ 0
IO2
Wire Wire Line
	8950 5150 9300 5150
$Comp
L power:+5V #PWR0101
U 1 1 604C7B76
P 6200 1850
F 0 "#PWR0101" H 6200 1700 50  0001 C CNN
F 1 "+5V" H 6215 2023 50  0000 C CNN
F 2 "" H 6200 1850 50  0001 C CNN
F 3 "" H 6200 1850 50  0001 C CNN
	1    6200 1850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 604D1058
P 6200 2150
F 0 "#PWR0102" H 6200 1900 50  0001 C CNN
F 1 "GND" H 6205 1977 50  0000 C CNN
F 2 "" H 6200 2150 50  0001 C CNN
F 3 "" H 6200 2150 50  0001 C CNN
	1    6200 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 1900 6200 1900
Wire Wire Line
	6200 1900 6200 1850
Wire Wire Line
	6050 2000 6200 2000
Wire Wire Line
	6200 2000 6200 2150
$Comp
L power:+3.3V #PWR?
U 1 1 6057229F
P 7000 950
AR Path="/6018463A/6057229F" Ref="#PWR?"  Part="1" 
AR Path="/6057229F" Ref="#PWR0103"  Part="1" 
F 0 "#PWR0103" H 7000 800 50  0001 C CNN
F 1 "+3.3V" H 7015 1123 50  0000 C CNN
F 2 "" H 7000 950 50  0001 C CNN
F 3 "" H 7000 950 50  0001 C CNN
	1    7000 950 
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 6057AB8C
P 7000 2300
AR Path="/6018463A/6057AB8C" Ref="#PWR?"  Part="1" 
AR Path="/6057AB8C" Ref="#PWR0104"  Part="1" 
F 0 "#PWR0104" H 7000 2150 50  0001 C CNN
F 1 "+3.3V" H 7015 2473 50  0000 C CNN
F 2 "" H 7000 2300 50  0001 C CNN
F 3 "" H 7000 2300 50  0001 C CNN
	1    7000 2300
	1    0    0    -1  
$EndComp
$EndSCHEMATC
